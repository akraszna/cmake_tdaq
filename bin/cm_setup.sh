#!/bin/echo "Please source this script, do not execute it"
# You can still debug it with 'sh -x cm_setup.sh ...'
#
# More general user setup script a la cmtsetup.sh
# It can setup both build and/or run-time environments, or just run-time environment.
# 
# If no configuration is given, a default one based on the OS version is chosen.
#
# Usage:
#
# source /path/to/cm_setup.sh [--runtime|-r]|[--build|-b] [--test|-t] <project>-<version> [ configuration ]
#
# Script:
#
# /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh
# 
# The script assumes a number of locations where to search.
# If this is not correct, it should be specified in TDAQ_RELEASE_BASE which will
# override all default paths.
#
# Examples:
#
# Setup only run-time environment, this is equivalent to sourcing the <project>/<version>/installed/setup.sh [configuration]# No build commands like cmake_config will be available.
# 
#     source /path/to/cm_setup.sh -r tdaq-06-02-00 x86_64-centos7-gcc62-opt
#     source /path/to/cm_setup.sh -r tdaq-06-02-00
#     source /path/to/cm_setup.sh -b tdaq-06-02-00
#     source /path/to/cm_setup.sh -t prod x86_64-slc6-gcc62-opt
# 
# If the -r option is not given, both the build followed by the run-time setup is done.
# If the -b option is given, only the build setup *for the given release* is done.
#    This is useful to get a specific cmake_tdaq version without setting up the run-time of the release itself.
# If the -t option is given, it will use the version of cmake_tdaq from where the cm_setup.sh comes from
#    This is useful for testing a new version, or overriding the version of an old release.
# 

if [ "${1}" = "--help" -o "${1}" = "-h" ]; then
    echo "usage: source ${BASH_SOURCE[0]:-${(%):-%x}} [-r|--runtime|-b|--build] [-t|--test] <project> [ <configuration> ]"
    echo "      <project> should be a versioned project name like tdaq-06-02-00"
    echo "      <configuration> should be a valid build configuration, like x86_64-slc6-gcc49-opt"
    echo
    echo "      The default is to setup the build environment, followed by the run-time environment"
    echo "      If no configuration is given a default configuration is chosen by the project itself"
    echo 
    echo "  -r|--runtime sets up only the run-time environment, not the build environment."
    echo "  -b|--build   sets up only the build environment of the selected release."
    echo
    echo "  These two options exclude each other."
    echo
    echo "  -t|--test    sets up the cmake_tdaq version from where cm_setup.sh comes from"
    
    return 0
fi

_cmake_tdaq=$(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})))

_candidates="${TDAQ_RELEASE_BASE:=/sw/atlas /afs/cern.ch/atlas/project/tdaq/inst /cvmfs/atlas-online-nightlies.cern.ch/tdaq/nightlies /cvmfs/atlas.cern.ch/repo/sw/tdaq /afs/cern.ch/atlas/project/tdaq/cmake/projects}"

_runtime_only=0
_build_only=0
_test_mode=0

while [ $# -gt 0 ]
do
    case $1 in
        -r|--runtime)
            _runtime_only=1
            shift
            ;;
        -b|--build)
            _build_only=1
            shift
            ;;
        -t|--test)
            _test_mode=1
            shift
            ;;
        *)
            break
            ;;
    esac
done

if [ $# -eq 0 ]; then
  _project=prod
else
  _project=$1
  shift
fi

case ${_project} in
    tdaq-common-*)
        _project_base=tdaq-common
        ;;
    dqm-common-*)
        _project_base=dqm-common
        ;;
    *)
        _project_base=tdaq
        ;;
esac

# Note: if no configuration is given but the project exists, we leave it to the
# project setup.sh script to choose a default version. This is better than hard-coding
# defaults for different versions in here.

_config=${1:-${CMTCONFIG}}

for _cand in $(echo ${_candidates}); do
    if [ -f ${_cand}/${_project_base}/${_project}/installed/setup.sh ]; then
        export TDAQ_RELEASE_BASE=${_cand}
        break
    fi
done

if [ ! -f ${TDAQ_RELEASE_BASE}/${_project_base}/${_project}/installed/setup.sh ]; then
    echo "Cannot find possible release candidate in: ${_candidates}"
    return 2
fi

if [ -z "${CMAKE_PROJECT_PATH}" ]; then
  export CMAKE_PROJECT_PATH=${TDAQ_RELEASE_BASE}
fi

if [ ! -z "${_config}" ]; then
  export CMTCONFIG=${_config}
fi

if [ ${_build_only} -eq 1 ]; then
    TDAQC_INST_PATH=$(${TDAQ_RELEASE_BASE}/${_project_base}/${_project}/installed/share/bin/run_tdaq printenv TDAQC_INST_PATH)
    echo "Using cmake_tdaq from ${TDAQC_INST_PATH}"
else
    source ${TDAQ_RELEASE_BASE}/${_project_base}/${_project}/installed/setup.sh ${_config}
fi

if [ ${_runtime_only} -eq 0 ];then
    case "${TDAQC_INST_PATH}" in
       */tdaq-common-02-00-00/*)
       # LCG closes AFS areas
          export LCG_RELEASE_BASE="/cvmfs/sft.cern.ch/lcg/releases"
          export CMAKE_PATH=/cvmfs/sft.cern.ch/lcg/contrib/CMake/3.6.0/Linux-x86_64/bin
          ;;
       */tdaq-common-02-02-00/*)
       # LCG closes AFS areas
          export LCG_RELEASE_BASE="/cvmfs/sft.cern.ch/lcg/releases"
          export CMAKE_VERSION=3.6.0
          ;;
    esac
    if [ ${_test_mode} -eq 0 -a -d ${TDAQC_INST_PATH}/share/cmake_tdaq ]; then
        source ${TDAQC_INST_PATH}/share/cmake_tdaq/bin/setup.sh ${_config}
    else
        source ${_cmake_tdaq}/bin/setup.sh ${_config}
    fi

    [ -n "${BASH}" ] && shopt -s nullglob
    [ -n "${ZSH_NAME}" ] && setopt null_glob

    _gdb_path=$(echo ${LCG_INST_PATH}/${TDAQ_LCG_RELEASE}/gdb/*/${CMTCONFIG})
    if [ -n "${_gdb_path}" ]; then
       export PATH=${_gdb_path}/bin:${PATH}
    fi
    unset _gdb_path

    [ -n "${BASH}" ] && shopt -u nullglob
    [ -n "${ZSH_NAME}" ] && setopt nonull_glob

    case "$CMTCONFIG" in
        x86_64-centos[78]-*)
            if [ -d /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7 ]; then
                export PATH=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/git/2.25.1/bin:$PATH
                export MANPATH=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7/git/2.25.1/man:$MANPATH
            fi
            ;;
        x86_64-slc6-*)
            if [ -d /cvmfs/sft.cern.ch/lcg/contrib/git/2.17.0 ]; then
                export PATH=/cvmfs/sft.cern.ch/lcg/contrib/git/2.17.0/x86_64-slc6/bin:$PATH
            fi
            ;;
    esac

fi

if [ -d /atlas/oks ]; then
    echo "Setting up environment specific to Point 1"
    export TDAQ_DB_PATH="/atlas/oks/${_project}":$TDAQ_DB_PATH
elif [ -d /tbed/git/oks/${CMTRELEASE} -a -f /tbed/tdaq/sw/oks/tdaq_db_repository.value ]; then
    echo "Setting up environment specific to test-bed (with OKS-GIT enabled)"
    function oks-git-on() { export TDAQ_DB_REPOSITORY=`envsubst < /tbed/tdaq/sw/oks/tdaq_db_repository.value` ; }
    function oks-git-off() { unset TDAQ_DB_REPOSITORY ; }
    function oks-git-status() { if [ -z ${TDAQ_DB_REPOSITORY} ] ; then echo "off" ; else echo "on" ; fi ; }
    oks-git-on
    export OKS_REPOSITORY_MAPPING_DIR="/tbed/git/oks/${CMTRELEASE}"
    export TDAQ_DB_PATH="$OKS_REPOSITORY_MAPPING_DIR:$TDAQ_DB_PATH"
    export TDAQ_IGUI_DB_GIT_BASE="http://pc-tbed-git.cern.ch:3000/atlas-gitea/${CMTRELEASE}/commit/"
    export TDAQ_DB_VERSION=""
    export TDAQ_IGUI_USE_BROWSER="firefox"
    # needed in case pmg agents are started by user at tbed
    export TDAQ_PMG_MANIFEST_AND_FIFOS_DIR="/var/atlas/${CMTRELEASE}"
elif [ -d /tbed/oks ]; then
    echo "Setting up environment specific to test-bed"
    export TDAQ_DB_PATH="/tbed/oks/${_project}":$TDAQ_DB_PATH
fi

if [ -d /tbed/tdaq/sw/ipc/${CMTRELEASE} ]; then
    export TDAQ_IPC_INIT_REF="file:/tbed/tdaq/sw/ipc/${CMTRELEASE}/ipc_root.ref"
fi


unset _cmake_tdaq _runtime_only _build_only _project _project_base_config _os _cand _candidates
