# Usage: source setup.sh [config]
#
# where 'config' is using the standard syntax: ARCH-OS-COMPILER-BUILD
# e.g. x86_64-slc6-gcc49-opt
#

# Latest default
lcg=LCG_97

case "$1" in
    --lcg=*)
        lcg=${1#--lcg=}
        shift
        ;;
    *)
        ;;
esac

############## configuration variables ################
# You can override this in your environment for testing other versions.

# legacy standard AFS location of LCG software
DEFAULT_LCG_BASE="/afs/cern.ch/sw/lcg/releases"
# favour new new standard location if CVMF available
[ -d /cvmfs/sft.cern.ch/lcg/releases ] && DEFAULT_LCG_BASE="/cvmfs/sft.cern.ch/lcg/releases"

case "${lcg}" in
    dev*)
       DEFAULT_LCG_BASE="/cvmfs/sft-nightlies.cern.ch/lcg/nightlies"
       ;;
esac

# you can still override this defining LCG_RELEASE_BASE beforehand
export LCG_RELEASE_BASE=${LCG_RELEASE_BASE:=${DEFAULT_LCG_BASE}}
LCG_BASE=$(dirname ${LCG_RELEASE_BASE})

# The location of LCG contrib and external
EXTERNAL_BASE=${EXTERNAL_BASE:=${LCG_BASE}/external}
CONTRIB_BASE=${CONTRIB_BASE:=${LCG_BASE}/contrib}
TOOL_BASE=${TOOL_BASE:=/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/x86_64-centos7}

# The location and version of CMake to use
CMAKE_BASE=${CMAKE_BASE:=${TOOL_BASE}/CMake}
CMAKE_VERSION=${CMAKE_VERSION:=3.16.4}
CMAKE_PATH=${CMAKE_PATH:=${CMAKE_BASE}/${CMAKE_VERSION}/bin}

# For TDAQ projects
CMAKE_PROJECT_PATH=${CMAKE_PROJECT_PATH:=/cvmfs/atlas.cern.ch/repo/sw/tdaq}

# export JAVA_HOME=${JAVA_HOME:=${EXTERNAL_BASE}/Java/JDK/1.8.0/amd64}

############## end of configuration variables ################

# Setup path for CMAKE
export PATH=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})):${CMAKE_PATH}:${PATH}

_conf=${1}
if [ -z "${_conf}" ]; then
   _conf=${CMTCONFIG}
fi

# Determine <arch>-<os>, used to find various paths for basic binutils and compilers
# without the need for the <compiler>-<opt|dbg> part of the tag.
CMAKE_ARCH=$(echo -n ${_conf} | cut -d- -f1,2)

# Choose compiler, this has to reflect what you choose in the tag to
# cmake_config later
case "${_conf}" in
    *-gcc*)
        if [ -n "${lcg}" ] && [ -f "${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt" ]; then
            gcc_requested=$(grep '^COMPILER:' ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt | cut -d' ' -f2 | cut -d';' -f2)
        else
            gcc_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'gcc' | sed 's;\(.\);\1.;g' | sed 's;\.$;;')
        fi
        if [ -d ${CONTRIB_BASE}/gcc/${gcc_requested}binutils ]; then
            source ${CONTRIB_BASE}/gcc/${gcc_requested}binutils/${CMAKE_ARCH}/setup.sh 
        else
            source ${CONTRIB_BASE}/gcc/${gcc_requested}/${CMAKE_ARCH}/setup.sh
        fi
        unset gcc_requested
        ;;
    *-clang*)
        if [ -n "${lcg}" ] && [ -f ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt ]; then
            clang_requested=$(grep '^COMPILER:' ${LCG_RELEASE_BASE}/${lcg}/LCG_externals_${_conf}.txt | cut -d' ' -f2 | cut -d';' -f2)
        else
            clang_requested=$(echo "${_conf}" | cut -d- -f3 | tr -d 'clang' | sed 's;\(.\);\1.;g' | sed 's;\.$;\.0;')
        fi
        source ${CONTRIB_BASE}/llvm/${clang_requested}/${CMAKE_ARCH}/setup.sh
        unset clang_requested
        ;;
    *)
        #default
        source ${CONTRIB_BASE}/gcc/8binutils/${CMAKE_ARCH}/setup.sh
        ;;
esac

export gcc_config_version
    
# Setup CMAKE_PREFIX_PATH to find LCG
export CMAKE_PREFIX_PATH=${CMAKE_PROJECT_PATH}:$(dirname $(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}})))/cmake

unset CMAKE_BASE CMAKE_VERSION CMAKE_PATH GCC_BASE CMAKE_ARCH LCG_BASE EXTERNAL_BASE CONTRIB_BASE TOOL_BASE _conf lcg
