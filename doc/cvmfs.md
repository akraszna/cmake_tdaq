
Running TDAQ from CVMFS
========================

From tdaq-07-00-00 onwards, the software should be usable directly from cvmfs. If done from
a remote site, there are a few things to consider, however. Some are due the read-only 
nature of CVMFS, others are due to the need to customize the system for your local 
environment.

There are several areas where the TDAQ release is installed: 

  * Once as stand-alone: `/cvmfs/atlas.cern.ch/repo/sw/tdaq`.
  * Once for every major off-line release, from 21 on: `/cvmfs/atlas.cern.ch/repo/sw/software/21.0`

The latter is fixed once it has been deployed, the former is regularly updated with patches.

The `cmake_tdaq` tools is available at `/cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq`,
so if you adjust your `cm_setup` alias to point there instead of AFS, you are all set.

CVMFS related customizations
----------------------------

By default the TDAQ software sets the variable `TDAQ_IPC_INIT_REF` to a location inside
the `${TDAQ_INST_PATH}/com` directory. This location is writable on AFS, so when the 
initial partition on the test bed is started, everybody will pick up the global IPC
server address from here. This is obviously not the case for CVMFS.

The user will have to set `TDAQ_IPC_INIT_REF` to a writable location. Where exactly
depends on the use case. E.g. for stand-alone tests that are fine with local IPC domain,
just put in somewhere where all nodes can see it (e.g. via NFS). For a pure localhost
partition, put it in `/tmp`, e.g.:

```shell
ipcdir=$(mktemp -d)
export TDAQ_IPC_INIT_REF=file:${ipcdir}/ipc_root.ref
```

Database related customizations
-------------------------------

The default database path (`$TDAQ_DB_PATH`) consists of two entries: one pointing
into the release area itself (`${TDAQ_INST_PATH}/share/data`) and one pointing to a
separate `databases` area. By default the latter is located in parallel to `${TDAQ_INST_PATH}`
and is maintained manually for the testbed and CERN by TDAQ experts. During an 
CVMFS installation this directory is not automatically added for off-line installs. It
is, however, there for the official TDAQ installation.

Since this area contains database files that are meant to be customized anyway, you should
get a private version. The easiest to use git and checkout the original package in
a local area. Use any of the following URLs, depending if you have Kerberos or SSH etc.

   * https://:@gitlab.cern.ch:8443/atlas-tdaq-software/databases.git (Kerberos)
   * ssh://git@gitlab.cern.ch:7999/atlas-tdaq-software/databases.git (ssh) 
   * https://gitlab.cern.ch/atlas-tdaq-software/databases.git

Then adjust the `TDAQ_DB_PATH` variable:

```shell
git clone https://:@gitlab.cern.ch:8443/atlas-tdaq-software/databases.git
export TDAQ_DB_PATH=${TDAQ_INST_PATH}/share/data:`pwd`/databases
```

Setting up the release for running
-----------------------------------

To run the release you only need the run-time setup, which you get by sourcing the `setup.sh`
script as usual.

```shell
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-07-01-00/installed/setup.sh
```

For tdaq-07-01-00 you can set the `CMTCONFIG` environment variable to a different configuration beforehand, otherwise
you get the built-in default (`x86_64-centos7-gcc8-opt`):

```shell
export CMTCONFIG=x86_64-slc6-gcc62-opt
```

Setting up for development
--------------------------

```shell
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-07-01-00 x86_64-slc6-gcc62-opt
```
