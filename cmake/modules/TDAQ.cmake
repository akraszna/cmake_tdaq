#######################################################################
#
# ATLAS TDAQ specific include file for CMake top level directories.
#
# It is assumed that this file is include()d from the CMakeLists.txt
# file in the top level working/project directory, with all packages
# checked out below it.
#
# The top level CMakeLists.txt file should look something like this
# for a project to be build:
#
#   cmake_minimum_required(VERSION 3.6.0)
#
#   find_package(TDAQ)
#   include(CTest)
#
#   tdaq_project(WorkArea daily USES tdaq 8.1.2)
#
# For a user work area, use the tdaq_work_area() macro instead
# of tdaq_project():
#
#   tdaq_work_area(tdaq 8.1.2)
# or after setup with cm_setup.sh preferably just:
#
#   tdaq_work_area()
#
#######################################################################

#######################################################################
# Global definitions. These mimic the CMT behaviour used so far.
#######################################################################

# cmake_policy(SET CMP0046 OLD)

if( "${CMAKE_VERSION}" VERSION_GREATER_EQUAL "3.12")
  cmake_policy(SET CMP0074 NEW)
  cmake_policy(SET CMP0094 NEW)
endif()

include(CMakeParseArguments)
include(CMakePackageConfigHelpers)

# Save this directory for later
get_filename_component(TDAQ_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR}/../.. ABSOLUTE)

# Options related to RPM dependency generation
option(TDAQ_HASHED_RPMS "Depend on hashed LCG RPMs" OFF)
option(TDAQ_SINGLE_RPM  "Depend on single LCG RPM" OFF)

# Options changing client build
option(TDAQ_SYSTEM_INCLUDE "When using upstream project, include with SYSTEM option" OFF)

set(TDAQ_DB_PROJECT daq)

# Stolen from ROOT since the one in ../cmake/modules/FindRoot does not work.
#----------------------------------------------------------------------------
# function ROOT_GENERATE_DICTIONARY( dictionary   
#                                    header1 header2 ... 
#                                    LINKDEF linkdef1 ... 
#                                    OPTIONS opt1...)
function(TDAQ_ROOT_GENERATE_DICTIONARY dictionary)

  if(NOT ROOT_LIBRARY_DIRS)
     find_package(ROOT QUIET)
     include_directories(${ROOT_INCLUDE_DIRS})
  endif()

  CMAKE_PARSE_ARGUMENTS(ARG "" "" "LINKDEF;OPTIONS" ${ARGN})
  #---Get the list of header files-------------------------
  set(headerfiles)
  foreach(fp ${ARG_UNPARSED_ARGUMENTS})
    if(NOT IS_ABSOLUTE ${fp} OR ${fp} MATCHES ${CMAKE_CURRENT_SOURCE_DIR}/.*)
      file(GLOB files RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${fp})
    elseif(${fp} MATCHES ${CMAKE_CURRENT_BINARY_DIR}/.*)
      file(GLOB files RELATIVE ${CMAKE_CURRENT_BINARY_DIR} ${fp})
    else()
      file(GLOB files ${fp})
    endif()
    if(files)
      foreach(f ${files})
        if(NOT f MATCHES LinkDef)
          set(headerfiles ${headerfiles} ${f})
        endif()
      endforeach()
    else()
      set(headerfiles ${headerfiles} ${fp})
    endif()
  endforeach()
  #---Get the list of include directories------------------
  get_directory_property(incdirs INCLUDE_DIRECTORIES)
  set(includedirs -I${CMAKE_CURRENT_SOURCE_DIR}) 
  foreach( d ${incdirs})    
   set(includedirs ${includedirs} -I${d})
  endforeach()
  #---Get LinkDef.h file------------------------------------
  set(linkdefs)
  foreach( f ${ARG_LINKDEF})
    if( IS_ABSOLUTE ${f})
      set(linkdefs ${linkdefs} ${f})
    else() 
      if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/inc/${f})
        set(linkdefs ${linkdefs} ${CMAKE_CURRENT_SOURCE_DIR}/inc/${f})
      else()
        set(linkdefs ${linkdefs} ${CMAKE_CURRENT_SOURCE_DIR}/${f})
      endif()
    endif()
  endforeach()
  #---call rootcint------------------------------------------
  add_custom_command(OUTPUT ${dictionary}.cpp ${dictionary}_rdict.pcm
                     COMMENT "Generating rootcling dictionary: ${dictionary}"
                     COMMAND env LD_LIBRARY_PATH=${ROOT_LIBRARY_DIRS}:${XZ_ROOT}/lib:$ENV{LD_LIBRARY_PATH} ${ROOT_BINARY_PATH}/rootcling -f ${dictionary}.cpp 
                                          ${ARG_OPTIONS} ${includedirs} ${headerfiles} ${linkdefs} 
                     DEPENDS ${headerfiles} ${linkdefs})
endfunction()

macro(tdaq_use_java)
  set(JAVA_HOME ${JAVA_ROOT} CACHE PATH "JAVA HOME")
  set(ENV{JAVA_HOME} ${JAVA_HOME})
  set(TDAQ_JAVA_HOME ${JAVA_ROOT} CACHE PATH "JAVA HOME")
  find_package(Java REQUIRED QUIET)
  find_package(JNI REQUIRED QUIET)
  include(UseJava)
endmacro()

######################################################################
# Write message to sw repository file
######################################################################
function(tdaq_sw_repo)
  foreach(msg ${ARGV})
    file(APPEND ${CMAKE_BINARY_DIR}/sw_repo.yaml ${msg} "\n")
  endforeach()
endfunction()

#######################################################################
# 
# tdaq_package([NO_HEADERS] [NO_DOCS])
#
# Every package should start with this at the top, e.g.:
#
#   tdaq_package()
#
# If a directory with the same name as the package name exists
# all header files from that directory are automatically installed.
#
# The variable TDAQ_PACKAGE_NAME is set to the name of the "current"
# package. The variables <package>_SOURCE_DIR and <package>_BINARY_DIR
# are injected into the parent scope pointing to this package's
# source and build directory rsp.
#
# The NO_HEADERS option means that no header files are automatically
# installed. Use tdaq_add_header_directory() to install them from
# a non-standard place.
#######################################################################
function(tdaq_package)
   cmake_parse_arguments(ARG "NO_HEADERS;NO_DOCS" "" "" ${ARGN})
   
   get_filename_component(TDAQ_PACKAGE_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)

   set(TDAQ_PACKAGE_VERSION"UNKNOWN")
   if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/.git)
     execute_process(COMMAND git describe --always --tags
       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
       OUTPUT_VARIABLE TDAQ_PACKAGE_VERSION
       OUTPUT_STRIP_TRAILING_WHITESPACE)
   elseif(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmt/version.cmt)
     execute_process(COMMAND cat cmt/version.cmt
       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
       OUTPUT_VARIABLE TDAQ_PACKAGE_VERSION
       OUTPUT_STRIP_TRAILING_WHITESPACE)
   endif()

   message(STATUS "  ${TDAQ_PACKAGE_NAME} : ${TDAQ_PACKAGE_VERSION}")
   file(APPEND ${CMAKE_BINARY_DIR}/packages.txt "${TDAQ_PACKAGE_NAME} ${TDAQ_PACKAGE_VERSION}\n")

   # weird, just setting it in the parent scope is not eough to use it later on
   set(TDAQ_COMPONENT_NOARCH ${TDAQ_PACKAGE_NAME}_noarch-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION})
   set(TDAQ_COMPONENT_NOARCH ${TDAQ_PACKAGE_NAME}_noarch-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION} PARENT_SCOPE)
   set(TDAQ_COMPONENT_BINARY ${TDAQ_PACKAGE_NAME}_${BINARY_TAG}-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION})
   set(TDAQ_COMPONENT_BINARY ${TDAQ_PACKAGE_NAME}_${BINARY_TAG}-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION} PARENT_SCOPE)

   file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/${TDAQ_PACKAGE_NAME}.version "${TDAQ_PACKAGE_VERSION}")
   install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TDAQ_PACKAGE_NAME}.version
     COMPONENT ${TDAQ_COMPONENT_NOARCH}
     DESTINATION share/cmake/pkg-versions
     OPTIONAL
     )

   set(TDAQ_PACKAGE_NAME ${TDAQ_PACKAGE_NAME} PARENT_SCOPE)
   set(TDAQ_PACKAGE_VERSION ${TDAQ_PACKAGE_VERSION} PARENT_SCOPE)
   set(TDAQ_PACKAGE_LABELS ${TDAQ_PACKAGE_NAME} PARENT_SCOPE)
   set(TDAQ_GENCONFIG_INCLUDES PARENT_SCOPE)
   add_definitions(-DTDAQ_PACKAGE_NAME=\"${TDAQ_PACKAGE_NAME}\")

   set(${TDAQ_PACKAGE_NAME}_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR} PARENT_SCOPE)
   set(${TDAQ_PACKAGE_NAME}_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR} PARENT_SCOPE)

   # do a dummy 'install' to trigger later creation of both binary and noarch RPMs:
   install(CODE "" COMPONENT  ${TDAQ_COMPONENT_NOARCH})
   install(CODE "" COMPONENT  ${TDAQ_COMPONENT_BINARY})

   if(NOT ARG_NO_HEADERS)
     if(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${TDAQ_PACKAGE_NAME})
       install(DIRECTORY ${TDAQ_PACKAGE_NAME} COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION include FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp" PATTERN ".git" EXCLUDE PATTERN ".svn" EXCLUDE)
     endif()
   endif()

   if(NOT ARG_NO_DOCS)
     if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/README.md)
       install(FILES README.md COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/doc/${TDAQ_PACKAGE_NAME})
     endif()
   endif()

   # Create the RPM spec files for this package
   if(EXISTS ${CMAKE_SOURCE_DIR}/cmake/template.spec.in)
     # string(REPLACE "-" "_" rpm_version "${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION}")
     set(rpm_version "${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION}")
     add_custom_command(OUTPUT template_binary.spec template_noarch.spec template_src.spec
       COMMAND sed -e "s/@TDAQ_PACKAGE_NAME@/${TDAQ_PACKAGE_NAME}/" -e "s/@BINARY_TAG@/${BINARY_TAG}/" -e "s/@TDAQ_PACKAGE_VERSION@/${rpm_version}/" ${CMAKE_SOURCE_DIR}/cmake/template.spec.in > ${CMAKE_CURRENT_BINARY_DIR}/template_binary.spec
       COMMAND sed -e "s/@TDAQ_PACKAGE_NAME@/${TDAQ_PACKAGE_NAME}/" -e "s/@BINARY_TAG@/noarch/" -e "s/@TDAQ_PACKAGE_VERSION@/${rpm_version}/" ${CMAKE_SOURCE_DIR}/cmake/template.spec.in > ${CMAKE_CURRENT_BINARY_DIR}/template_noarch.spec
       COMMAND sed -e "s/@TDAQ_PACKAGE_NAME@/${TDAQ_PACKAGE_NAME}/" -e "s/@BINARY_TAG@/src/" -e "s/@TDAQ_PACKAGE_VERSION@/${rpm_version}/" ${CMAKE_SOURCE_DIR}/cmake/template.spec.in > ${CMAKE_CURRENT_BINARY_DIR}/template_src.spec
       DEPENDS ${CMAKE_SOURCE_DIR}/cmake/template.spec.in)
     add_custom_target(${TDAQ_PACKAGE_NAME}_spec ALL
       DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/template_binary.spec ${CMAKE_CURRENT_BINARY_DIR}/template_noarch.spec ${CMAKE_CURRENT_BINARY_DIR}/template_src.spec)
   endif()

endfunction(tdaq_package)

# ######################################################################
# Find include files
# - if P is an existing file, accept it as is.
# - else:
#      find_package(A QUIET)
#      if A_FOUND
#        add A_INCLUDE_DIRS to includes
# ######################################################################   
function(_tdaq_resolve_includes result)
   set(headers)

   foreach(lib ${ARGN})
     if(DEFINED TDAQ_HAVE_${lib} OR IS_DIRECTORY ${CMAKE_SOURCE_DIR}/${lib})
       set(headers ${headers} $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/${lib}>)
       set(headers ${headers} $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/${lib}>)
     endif()
     if(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${lib})
       set(headers ${headers} $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/${lib}>)
     elseif(IS_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${lib})
       set(headers ${headers} $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${lib}>)
     elseif(IS_DIRECTORY ${CMAKE_SOURCE_DIR}/${lib}/${lib})
       set(headers ${headers} $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/${lib}>)
     elseif("${lib}" STREQUAL "PRIVATE")
       set(headers ${headers} ${lib})
     elseif("${lib}" STREQUAL "PUBLIC")
       set(headers ${headers} PUBLIC)
     elseif("${lib}" STREQUAL "INTERFACE")
       set(headers ${headers} INTERFACE)
     elseif("${lib}" MATCHES "/.*")
       set(headers ${headers} ${lib})
     else()
       # message(STATUS "    Searching for headers of ${lib}")
       find_package(${lib} QUIET)
       if("${lib}" STREQUAL "PythonLibs")
          set(lib PYTHONLIBS)
       endif()
       if(${lib}_FOUND)
            # message(STATUS "Found headers of ${lib}")
            if("${lib}" STREQUAL "PYTHONLIBS")
              set(lib PYTHON)
            endif()
            # message(STATUS "Including ${${lib}_INCLUDE_DIRS}")
          set(headers ${headers} ${${lib}_INCLUDE_DIRS})
       elseif(NOT DEFINED PRJ_WORK)
          message(STATUS "Includes not found: ${lib}")
       endif()
     endif()
   endforeach()
   if(headers)
     list(REMOVE_DUPLICATES headers)
   endif()
   set(${result} ${headers} PARENT_SCOPE)
endfunction()

# ######################################################################
# Resolve target link libraries of the from P or P::C
# - if P is a target, accept it as is.
# - if P is an existing file, accept it as is.
# - if P is a simple name:
#      find_package(P QUIET)
#      if P_FOUND
#        add P_LIBRARIES to the result
#        add P_INCLUDE_DIRS to includes
# - if P is of the form P::C
#      find_package(P QUIET COMPONENTS C)
#      if(P_C_FOUND)
#        add P_INCLUDE_DIRS to includes
#        add P_C_LIBRARY to the result
# ######################################################################
function(_tdaq_resolve_libraries result includes)
  set(libs)
  set(headers)
  foreach(lib ${ARGN})
    if(TARGET ${lib})
      set(libs ${libs} ${lib})
    elseif(EXISTS ${lib})
      set(libs ${libs} ${lib})
    elseif("${lib}" STREQUAL "PRIVATE")
      set(libs ${libs} ${lib})
      set(headers ${headers} ${lib})
    elseif("${lib}" STREQUAL "PUBLIC")
      set(libs ${libs} ${lib})
      set(headers ${headers} ${lib})
    elseif("${lib}" STREQUAL "INTERFACE")
      set(libs ${libs} ${lib})
      set(headers ${headers} ${lib})
    else()

      if(${BOOST_VERSION} VERSION_GREATER_EQUAL 1.69 AND ${lib} STREQUAL "Boost::python")
        string(REGEX REPLACE "^([0-9]+)\.([0-9]+).*" "\\1\\2" py_version ${PYTHON_VERSION})
        set(lib "Boost::python${py_version}")
      endif()

      if(${lib} MATCHES .*::.*)
         string(REPLACE :: ";" out ${lib})
         list(GET out 0 pkg)
         list(GET out 1 component)
         # message(STATUS "    Searching for ${pkg}::${component}")
         find_package(${pkg} QUIET COMPONENTS ${component})
         if(${pkg}_FOUND)
           # After this call, the package may have imported targets
           # which are just what we are looking for.
           if(TARGET ${lib})
             # For an imported target we assume it has all include
             # and library dependencies
             set(libs ${libs} ${lib})
           else()
             set(upper)
             string(TOUPPER ${component} upper)
             #if(${pkg}_{component}_FOUND) OR ${pkg}_${upper}_FOUND)p
             set(headers ${headers} ${${pkg}_INCLUDE_DIRS} ${${upper}_INCLUDE_DIRS})
             set(libs ${libs} ${${pkg}_${component}_LIBRARY} ${${pkg}_${upper}_LIBRARY} ${${pkg}_LIBRARIES})
             #else()
             #  message(STATUS "Component not found ${pkg}::${component}")
             #  set(libs ${libs} ${lib})
             #endif()
             if(${pkg} STREQUAL "ROOT")
               find_package(TBB REQUIRED QUIET)
               set(headers ${headers} ${TBB_INCLUDE_DIRS})
               set(libs ${libs} ${TBB_LIBRARY})
               find_package(LibLZMA QUIET)
               if(LIBLZMA_FOUND)
                 set(headers ${headers} ${LIBLZMA_INCLUDE_DIRS})
                 set(libs ${libs} ${LIBLZMA_LIBRARIES})
               endif()
             endif()

           endif()
         else()
            set(libs ${libs} ${lib})
         endif()
      else()
         # message(STATUS "    Searching for ${lib}")
         find_package(${lib} QUIET)
         set(upper)
         string(TOUPPER ${lib} upper)
         if(${lib}_FOUND OR ${upper}_FOUND)
            if("${lib}" STREQUAL "PythonLibs")
              set(lib PYTHON)
            endif()
            set(libs ${libs} ${${lib}_LIBRARIES} ${${upper}_LIBRARIES})
            set(headers ${headers} ${${lib}_INCLUDE_DIRS} ${${upper}_INCLUDE_DIRS})

	    if(${lib} STREQUAL "ROOT")
               find_package(LibLZMA QUIET)
               if(LIBLZMA_FOUND)
                 set(headers ${headers} ${LIBLZMA_INCLUDE_DIRS})
                 set(libs ${libs} ${LIBLZMA_LIBRARIES})
               endif()
	    endif()
         else()
           set(libs ${libs} ${lib})
         endif()
      endif()
    endif()
  endforeach()
  if(libs)
     list(REMOVE_DUPLICATES libs)
  endif()
  if(headers)
    list(REMOVE_DUPLICATES headers)
  endif()
  set(${result} ${libs} PARENT_SCOPE)
  set(${includes} ${headers} PARENT_SCOPE)
endfunction()

#######################################################################
#
# tdaq_add_library(libname src1 src2... 
#                  [NOINSTALL][DAL] 
#                  [LINK_LIBRARIES lib1 lib2...] 
#                  [INCLUDE_DIRECTORIES dir1 dir2...]
#                  [DEFINITIONS A B=C...]
#                  [OPTIONS -O3 ...])
# 
# Use this function to add a library to the build. It will be automatically
# installed according to the standard TDAQ rules.
#
# Each <srcN> argument will be glob'ed before being added to the target source list.
# So you can use wild-card expression.
# 
# If a file name ends in *.idl, the tdaq_add_idl() function will be invoked and
# the generated sources will be added to the library, the required includes are
# added with target_include_directories().
#
# The following additional options are available compared to the standard
# add_library():
#
#    NOINSTALL           - option, if given the library will be build but not installed.
#    DAL                 - this is a DAL library
#    IDL_DEPENDS         - list of additional IDL dependencies, these should just specify
#      the IDL interface without path or .idl, e.g. IDL_DEPENDS rdb ipc
#      ISInfo header files are used.
#    LINK_LIBRARIES      - a list of libraries to link against.
#    INCLUDE_DIRECTORIES - a list of include directories to add
#    DEFINITIONS         - a list of preprocessor definitions to add
#    OPTIONS             - a list of compiler options to add
#    DEPENDS             - a list of additional dependencies
#
# The last four options are also available by using the usual CMake functions:
#
# target_include_directories(), target_link_libraries(), target_compiler_definitions() 
# and target_compiler_options()
#
# You can specify PUBLIC, PRIVATE or INTERFACE, default is PUBLIC for these options
# just as in standard CMake.
#
#######################################################################

function(tdaq_add_library name)

   list(FIND TDAQ_DISABLED_TARGETS ${name} _disabled)
   if(NOT ${_disabled} EQUAL -1)
     return()
   endif()

   set(LIBOPTS_LINK_LIBRARIES)                                                 
   cmake_parse_arguments(LIBOPTS "NOINSTALL;DAL;SHARED;STATIC;MODULE;OBJECTS;INTERFACE" "DESTINATION" "LINK_LIBRARIES;INCLUDE_DIRECTORIES;DEFINITIONS;OPTIONS;IDL_DEPENDS;DEPENDS" ${ARGN})

   if(LIBOPTS_DESTINATION)
     set(lib_dest ${LIBOPTS_DESTINATION})
   elseif(TDAQ_LIB_DESTINATION)
     set(lib_dest ${TDAQ_LIB_DESTINATION})
   else()
     set(lib_dest ${BINARY_TAG}/lib)
   endif()

   set(libtype SHARED)
   if(LIBOPTS_STATIC)
      set(libtype STATIC)
   elseif(LIBOPTS_MODULE)
      set(libtype MODULE)
   elseif(LIBOPTS_OBJECTS)
      set(libtype OBJECTS)
    elseif(LIBOPTS_INTERFACE)
      set(libtype INTERFACE)
   endif()

   set(srcs)
   set(includes)
   set(idl_deps)
   foreach(f ${LIBOPTS_UNPARSED_ARGUMENTS})
     if(${f} MATCHES ".+\\.idl")
       set(idl_srcs)
       set(idl_includes)

       tdaq_add_idl(${f} DEPENDS ${LIBOPTS_IDL_DEPENDS} CPP_OUTPUT idl_srcs INCLUDE_OUTPUT idl_includes)
       list(APPEND srcs ${idl_srcs})
       list(APPEND includes ${idl_includes})
       get_filename_component(idl_base ${f} NAME_WE)
       list(APPEND idl_deps IDL_${idl_base})
     elseif(${f} MATCHES ".*\\*.*")
       set(out)
       file(GLOB out ${f})
       set(srcs ${srcs} ${out})
     else()
       # may be generated file, so just add
       set(srcs ${srcs} ${f})
     endif()
   endforeach()

   add_library(${name} ${libtype} ${srcs})
   if(NOT "${libtype}" STREQUAL "INTERFACE")
     set_target_properties(${name} PROPERTIES LABELS "${TDAQ_PACKAGE_LABELS}")
   endif()

   if(NOT "${idl_deps}" STREQUAL "")
     add_dependencies(${name} ${idl_deps})
   endif()

   if(NOT (${libtype} STREQUAL "INTERFACE" OR ${libtype} STREQUAL "STATIC"))
     if(TDAQ_NO_DEBUG_INSTALL)
       add_custom_command(TARGET ${name} POST_BUILD
         COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${name}>)
     else()
       add_custom_command(TARGET ${name} POST_BUILD
         COMMAND ${CMAKE_OBJCOPY} --only-keep-debug $<TARGET_FILE:${name}> ${CMAKE_CURRENT_BINARY_DIR}/lib${name}.so.debug
         COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${name}>
         COMMAND ${CMAKE_OBJCOPY} --add-gnu-debuglink=lib${name}.so.debug  $<TARGET_FILE:${name}>)
       get_directory_property(clean_files ADDITIONAL_MAKE_CLEAN_FILES)
       list(APPEND clean_files ${CMAKE_CURRENT_BINARY_DIR}/lib${name}.so.debug)
       set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES "${clean_files}")
     endif()
   endif()

   if(NOT LIBOPTS_NOINSTALL)
     set(TDAQ_TARGETS ${TDAQ_TARGETS} ${name} PARENT_SCOPE)
     install(TARGETS ${name} OPTIONAL COMPONENT ${TDAQ_COMPONENT_BINARY} EXPORT ${PROJECT_NAME} LIBRARY DESTINATION ${lib_dest} ARCHIVE DESTINATION ${lib_dest})
     if(NOT (${libtype} STREQUAL "INTERFACE" OR ${libtype} STREQUAL "STATIC"))
       if(NOT TDAQ_NO_DEBUG_INSTALL)
         install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${name}.so.debug OPTIONAL COMPONENT ${TDAQ_COMPONENT_BINARY} DESTINATION ${lib_dest})
       endif()
       endif()
   endif()
   if(LIBOPTS_INTERFACE) 
     target_include_directories(${name} INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}> ${includes})     
   else()
     target_include_directories(${name} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}> ${includes})
   endif()

   if(LIBOPTS_DAL)
     target_include_directories(${name} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>)
     if(TARGET DAL_${TDAQ_PACKAGE_NAME})
       add_dependencies(${name} DAL_${TDAQ_PACKAGE_NAME})
     endif()
   endif()

   if(LIBOPTS_INCLUDE_DIRECTORIES)
      # message(STATUS "    Adding ${LIBOPTS_INCLUDE_DIRECTORIES}")
      _tdaq_resolve_includes(include_headers ${LIBOPTS_INCLUDE_DIRECTORIES})
      # message(STATUS "    Adding resolved: ${include_headers}")
      if(${libtype} STREQUAL "INTERFACE")
        target_include_directories(${name} INTERFACE ${include_headers})
      else()
        target_include_directories(${name} PUBLIC ${include_headers})
      endif()
   endif()

   if(LIBOPTS_LINK_LIBRARIES)
       _tdaq_resolve_libraries(link_libraries link_headers ${LIBOPTS_LINK_LIBRARIES})
       if(${libtype} STREQUAL "INTERFACE")
         target_link_libraries(${name} INTERFACE ${link_libraries})
       else()
         target_link_libraries(${name} PUBLIC ${link_libraries})
       endif()
       if(NOT "${link_headers}" STREQUAL "")
         if(${libtype} STREQUAL "INTERFACE")
           target_include_directories(${name} INTERFACE ${link_headers})
         else()
           target_include_directories(${name} PUBLIC ${link_headers})
         endif()
       endif()
   endif()

   foreach(idl_target ${LIBOPTS_IDL_DEPENDS})
     if(DEFINED TDAQ_HAVE_${idl_target})
       target_include_directories(${name} PUBLIC $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}/${idl_target}/idl>)
       add_dependencies(${name} IDL_${idl_target})
     endif()
   endforeach()

   if(LIBOPTS_DEFINITIONS)
       target_compile_definitions(${name} PUBLIC ${LIBOPTS_DEFINITIONS})
   endif()

   if(LIBOPTS_OPTIONS)
       target_compile_options(${name} PUBLIC ${LIBOPTS_OPTIONS})
   endif()

   if(LIBOPTS_DEPENDS)
       add_dependencies(${name} ${LIBOPTS_DEPENDS})
   endif()

endfunction(tdaq_add_library)

#######################################################################
#
# tdaq_add_executable(exename src1 src2.. 
#                     [NOINSTALL] 
#                     [INCLUDE_DIRECTORIES dir1 dir2..]
#                     [LINK_LIBRARIES lib1 lib2...]
#                     [DEFINITIONS A B=C ...]
#                     [OPTIONS -pthread ...]
#                     [OUTPUT_NAME new_name]
#
# Add an executable to the build and install it according in the TDAQ
# standard locations.
#
# The following additional options are available compared to the standard
# add_executable():
#
#    NOINSTALL           - option, if given the library will be build but not installed.
#    DAL                 - this is a DAL executable
#    LINK_LIBRARIES      - a list of libraries to link against.
#    INCLUDE_DIRECTORIES - a list of include directories to add
#    DEFINITIONS         - a list of preprocessor definitions to add
#    OPTIONS             - a list of compiler options to add
#    DEPENDS             - a list of additional dependencies
#    OUTPUT_NAME         - rename the executable when install it
#    ADD_TO_OKS          - add binary to OKS software repository
#    DESCRIPTION         - add description to binary in OKS software repository
# 
#
# The last four options are also available by the usual CMake functions:
#
# target_include_directories(), target_link_libraries(), target_compiler_definitions() 
# and target_compiler_options()
#
# You can specify PUBLIC, PRIVATE or INTERFACE, default is PRIVATE for these options.
#
#######################################################################
function(tdaq_add_executable name)

   list(FIND TDAQ_DISABLED_TARGETS ${name} _disabled)
   if(NOT ${_disabled} EQUAL -1)
     return()
   endif()

   set(BINOPTS_LINK_LIBRARIES)                                                 
   cmake_parse_arguments(BINOPTS "NOINSTALL;DAL;ADD_TO_OKS" "OUTPUT_NAME;DESTINATION;DESCRIPTION" "LINK_LIBRARIES;INCLUDE_DIRECTORIES;DEFINITIONS;OPTIONS;DEPENDS" ${ARGN})

   if(BINOPTS_DESTINATION)
     set(bin_dest ${BINOPTS_DESTINATION})
   elseif(TDAQ_BIN_DESTINATION)
     set(bin_dest ${TDAQ_BIN_DESTINATION})
   else()
     set(bin_dest ${BINARY_TAG}/bin)
   endif()

   set(srcs)
   foreach(f ${BINOPTS_UNPARSED_ARGUMENTS})
     set(out)
     file(GLOB out ${f})
     if(out)
        set(srcs ${srcs} ${out})
     else()
        set(srcs ${srcs} ${f})
     endif()
   endforeach()

   add_executable(${name} ${srcs})
   set_target_properties(${name} PROPERTIES LABELS "${TDAQ_PACKAGE_LABELS}")

   set(debug_name ${name}.debug)
   if(BINOPTS_OUTPUT_NAME)
     set(debug_name ${BINOPTS_OUTPUT_NAME}.debug)
     set_target_properties(${name} PROPERTIES RUNTIME_OUTPUT_NAME ${BINOPTS_OUTPUT_NAME})
   endif()

   if(TDAQ_NO_DEBUG_INSTALL)
     add_custom_command(TARGET ${name} POST_BUILD
       COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${name}>)
   else()
     add_custom_command(TARGET ${name} POST_BUILD
       COMMAND ${CMAKE_OBJCOPY} --only-keep-debug $<TARGET_FILE:${name}> ${CMAKE_CURRENT_BINARY_DIR}/${debug_name}
       COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${name}>
       COMMAND ${CMAKE_OBJCOPY} --add-gnu-debuglink=${debug_name} $<TARGET_FILE:${name}>)
   endif()

   get_directory_property(clean_files ADDITIONAL_MAKE_CLEAN_FILES)
   list(APPEND clean_files ${CMAKE_CURRENT_BINARY_DIR}/${debug_name})
   set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES "${clean_files}")

   if(BINOPTS_DAL)
     target_include_directories(${name} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>)
     if(TARGET DAL_${TDAQ_PACKAGE_NAME})
       add_dependencies(${name} DAL_${TDAQ_PACKAGE_NAME})
     endif()
   endif()

   if(NOT BINOPTS_NOINSTALL)
     set(TDAQ_TARGETS ${TDAQ_TARGETS} ${name} PARENT_SCOPE)
     install(TARGETS ${name} OPTIONAL COMPONENT ${TDAQ_COMPONENT_BINARY} EXPORT ${PROJECT_NAME} RUNTIME DESTINATION ${bin_dest})
     if(NOT TDAQ_NO_DEBUG_INSTALL)
       install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${debug_name} OPTIONAL COMPONENT ${TDAQ_COMPONENT_BINARY} DESTINATION ${bin_dest})
     endif()
   endif()

   if(BINOPTS_LINK_LIBRARIES)
       _tdaq_resolve_libraries(link_libraries link_headers ${BINOPTS_LINK_LIBRARIES})
       target_link_libraries(${name} PRIVATE ${link_libraries})
       target_include_directories(${name} PRIVATE ${link_headers})
   endif()

   if(BINOPTS_INCLUDE_DIRECTORIES)
      _tdaq_resolve_includes(link_headers ${BINOPTS_INCLUDE_DIRECTORIES})
      target_include_directories(${name} PRIVATE ${link_headers})
   endif()

   if(BINOPTS_DEFINITIONS)
       target_compile_definitions(${name} PRIVATE ${BINOPTS_DEFINITIONS})
   endif()

   if(BINOPTS_OPTIONS)
       target_compile_options(${name} PRIVATE ${BINOPTS_OPTIONS})
   endif()

   if(BINOPTS_DEPENDS)
       add_dependencies(${name} ${BINOPTS_DEPENDS})
   endif()

endfunction(tdaq_add_executable)

# ######################################################################
# _tdaq_use_project(name version)
#
# Find and use the specified project and version
#
# ######################################################################
macro(_tdaq_use_project name version)

  file(TO_CMAKE_PATH "$ENV{CMAKE_PREFIX_PATH}" dirs)
  set(paths)
  foreach(dir ${dirs})
    set(paths ${paths} ${dir}/${name}/*/installed)
  endforeach()
  
  find_package(${name} ${version} EXACT REQUIRED CONFIG PATHS ${paths} PATH_SUFFIXES ${BINARY_TAG})
  
  if(${name}_FOUND)
    message(STATUS "Found project: ${name}/${version} in ${${name}_DIR}")
    if(TDAQ_SYSTEM_INCLUDE)
      include_directories(SYSTEM ${${name}_INCLUDE_DIRS})
    else()
      include_directories(${${name}_INCLUDE_DIRS})
    endif()
    # link_directories(${${name}_LIBRARY_DIRS})
  else()
    message(FATAL_ERROR "Project not found in CMAKE_PREFIX_PATH: ${name} ${version}")
  endif()
  
endmacro(_tdaq_use_project)

#
# Build a TDAQ standard string from name and version
#
function(_tdaq_version_to_name result name version)
   if(${version} MATCHES [0-9]+)
      # must have three components
      set(out)
      string(REPLACE "." ";" out ${version})
      set(out_version)
      foreach(ver ${out})
         if(${ver} LESS 10)
             set(out_version "${out_version}-0${ver}")
         else()
             set(out_version "${out_version}-${ver}")
         endif()
      endforeach()
      set(${result} "${name}${out_version}" PARENT_SCOPE)
   else()
     if(${name} STREQUAL "tdaq")
       set(${result} "${version}" PARENT_SCOPE)
     else()
       set(${result} "${name}-${version}" PARENT_SCOPE)
     endif()
   endif()
endfunction()

macro(tdaq_set_build_flags)
  # We always want shared libraries as default.
  set(BUILD_SHARED_LIBS true)

  # The current directory should be always on the include path.
  set(CMAKE_INCLUDE_CURRENT_DIR true)

  # Directories should always be added *before* the current path
  set(CMAKE_INCLUDE_DIRECTORIES_PROJECT_BEFORE ON)

  # Common definitions and flags
  add_definitions(-D_GNU_SOURCE -D_REENTRANT -D__USE_XOPEN2K8)
  if(${CMAKE_BUILD_TYPE} STREQUAL "Release")
     add_definitions(-DERS_NO_DEBUG)
  endif()

  # set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y")

  # The install directory is in your top level directory called 'installed'
  if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX ${CMAKE_SOURCE_DIR}/installed CACHE PATH "Install path prefix, prepended onto install directories." FORCE)
  endif()

  # Debug/Release and architecture independent
  # this is for gcc only
  
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -g -pedantic -pipe -Wall -Wwrite-strings")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -pedantic -pipe -Wall -Wwrite-strings -Wno-unused-local-typedefs -Wno-register -DBOOST_BIND_GLOBAL_PLACEHOLDERS=1 ")
  if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang") 
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-mismatched-tags -Wno-c11-extensions -Wno-overloaded-virtual -Wno-incompatible-pointer-types-discards-qualifiers -Wno-inconsistent-missing-override")
  endif()

  set(CMAKE_Fortan_FLAGS "${CMAKE_Fortran_FLAGS} -g -pedantic -pipe -Wall -Wwrite-strings")

  # 32bit only
  if(${LCG_platform} MATCHES i686-*)
     set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -m32 -march=i686")
     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -m32  -march=i686")
     set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -m32  -march=i686")
     set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -m32")
     set(CMAKE_SHARED_LINKER_FLAGS_RELEASE "${CMAKE_SHARED_LINKER_FLAGS_RELEASE} -m32")
  endif()

  # only for gcc >= 7 for now
  set(CMAKE_CXX_STANDARD 17 CACHE STRING "Select C++17" FORCE)

  if(${CMAKE_SYSTEM_PROCESSOR} MATCHES "x86_64")
    set(TDAQ_OPT_FLAGS " -mtune=intel ")
  endif()

  set(CMAKE_C_FLAGS_RELEASE "-O2 ${TDAQ_OPT_FLAGS} -ftree-vectorize -fomit-frame-pointer")
  set(CMAKE_CXX_FLAGS_RELEASE "-O2 ${TDAQ_OPT_FLAGS} -ftree-vectorize -fomit-frame-pointer")
  set(CMAKE_Fortran_FLAGS_RELEASE "-O2 ${TDAQ_OPT_FLAGS} -ftree-vectorize -fomit-frame-pointer")

  set(CMAKE_C_FLAGS_DEBUG "-O0 -DDEBUG_LEVEL=1")
  set(CMAKE_CXX_FLAGS_DEBUG "-O0 -DDEBUG_LEVEL=1")
  set(CMAKE_Fortran_FLAGS_DEBUG "-O0")

endmacro(tdaq_set_build_flags)

#########endmacro##########################################
# Helper function to import targets from release
# This is a separate function because we have to
# do various find_package() calls so that the imported
# targets are available
# ##################################################
function(tdaq_import_targets prj_name)
  string(TOUPPER ${CMAKE_BUILD_TYPE} build_type)
  foreach(prj_target ${${prj_name}_TARGETS})
    if(NOT TARGET ${prj_target})
      get_target_property(target_type ${prj_name}::${prj_target} TYPE)
      get_target_property(target_name ${prj_name}::${prj_target} NAME)

      set_target_properties(${prj_name}::${prj_target} PROPERTIES IMPORTED_GLOBAL TRUE)

      if(${target_type} STREQUAL "EXECUTABLE")
        add_executable(${prj_target} ALIAS ${prj_name}::${prj_target})
      elseif(${target_type} STREQUAL "SHARED_LIBRARY" OR ${target_type} STREQUAL "MODULE_LIBRARY")
        add_library(${prj_target} ALIAS ${prj_name}::${prj_target})
        get_target_property(target_link ${prj_name}::${prj_target} INTERFACE_LINK_LIBRARIES)
        if(target_link)
          set(target_link_out)
          foreach(t ${target_link})
            string(REPLACE "${prj_name}::" "" u ${t})
            if(${u} MATCHES .*::.*)
              if(NOT TARGET ${u})
                string(REGEX REPLACE "::.*" "" pkg ${u})
                string(REGEX REPLACE ".*::" "" comp ${u})
                find_package(${pkg} QUIET COMPONENTS ${comp})
              endif()
            endif()
            set(target_link_out ${target_link_out} ${u})

          endforeach()
          # message(STATUS "Adding to libs ${prj_target}: ${target_link_out}")
          set_target_properties(${prj_name}::${prj_target} PROPERTIES INTERFACE_LINK_LIBRARIES "${target_link_out}")
        endif()
      elseif(${target_type} STREQUAL "INTERFACE_LIBRARY")
        add_library(${prj_target} ALIAS ${prj_name}::${prj_target})
      endif()
    endif()
  endforeach()
endfunction()

function(_tdaq_release_to_project release project version)
  execute_process(COMMAND ${TDAQ_CMAKE_DIR}/cmake/scripts/release2cmake ${release}
    OUTPUT_VARIABLE output OUTPUT_STRIP_TRAILING_WHITESPACE)
  string(REPLACE " " ";" output ${output})
  list(GET output 0 _project)
  list(GET output 1 _version)
  set(${project} ${_project} PARENT_SCOPE)
  set(${version} ${_version} PARENT_SCOPE)
endfunction()

######################################################################
#
# tdaq_work_area(project version)
# 
# A user work area should contain a simple CMakeLists.txt with
# only the following lines:
#
# cmake_minimum_required(VERSION 3.4.3)
# include(TDAQ)
# tdaq_work_area(tdaq 5.3.0)
#
# Optionally you can do a cm_setup with a given release and
# let it automatically choose the dependent project.
#
# tdaq_work_area()
#
######################################################################
macro(tdaq_work_area)
   include(CTest)

   set(_project)
   set(_version)

   if(${ARGC} EQUAL 2)
     tdaq_project(${CMAKE_PROJECT_NAME} ${ARGV1} WORK USES ${ARGV0} ${ARGV1})
   elseif(${ARGC} EQUAL 1)
     _tdaq_release_to_project(${ARGV0} _project _version)
     tdaq_project(${CMAKE_PROJECT_NAME} ${_version} WORK USES ${_project} ${_version})
   else()
     _tdaq_release_to_project($ENV{CMTRELEASE} _project _version)
     tdaq_project(${CMAKE_PROJECT_NAME} ${_version} WORK USES ${_project} ${_version})
   endif()
endmacro()

#######################################################################
# 
# tdaq_project(name version [ USES project version...] [ EXTERNALS ext1 ext2...])
# 
# This should be only used in a top level CMakeLists.txt file for
# a work area or a project (like tdaq-common, dqm-common, tdaq).
#
# The version should be in the format major.minor.patch
#
# The EXTERNAL parameters are used for RPM dependencies for LCG externals.
#
#######################################################################

function(tdaq_project name version)

  cmake_parse_arguments(PRJ "WORK" "" "USES;EXTERNALS;IMPORT" ${ARGN})

  if(EXISTS ${CMAKE_SOURCE_DIR}/.git)
    execute_process(COMMAND git rev-parse HEAD
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
      OUTPUT_VARIABLE TDAQ_COMMIT
      OUTPUT_STRIP_TRAILING_WHITESPACE)
    message(STATUS "Building commit ${TDAQ_COMMIT}")
    file(WRITE ${CMAKE_BINARY_DIR}/COMMIT ${TDAQ_COMMIT})
    install(FILES ${CMAKE_BINARY_DIR}/COMMIT OPTIONAL COMPONENT WithoutLCG_${BINARY_TAG} DESTINATION share/cmake/${name}/${BINARY_TAG})

    execute_process(COMMAND ${TDAQ_CMAKE_DIR}/cmake/scripts/get_revision ${name}
      WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
      OUTPUT_VARIABLE TDAQ_REVISION
      OUTPUT_STRIP_TRAILING_WHITESPACE)
  endif()

  set(CMAKE_DEPENDS_IN_PROJECT_ONLY TRUE)

  set(USES ${PRJ_USES})
  set(PROJECT_LIST)
  # set(BUILDNAME ${BINARY_TAG} CACHE STRING "CDash build name" FORCE)

  set(Boost_NO_BOOST_CMAKE 1)

  if(IS_DIRECTORY ${CMAKE_SOURCE_DIR}/cmake/modules)
    list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake/modules)
  endif()

  if(IS_DIRECTORY ${CMAKE_SOURCE_DIR}/cmake)
    list(APPEND CMAKE_PREFIX_PATH  ${CMAKE_SOURCE_DIR}/cmake)
  endif()

  include("tdaq_user_prefix.cmake" OPTIONAL)

  string(REGEX MATCHALL "[^-]+" config_list ${BINARY_TAG})
  list(GET config_list 0 arch)
  list(GET config_list 1 os)
  list(GET config_list 2 cxx)
  list(GET config_list 3 mode)

  include("tdaq_user_prefix-${arch}.cmake" OPTIONAL)
  include("tdaq_user_prefix-${os}.cmake" OPTIONAL)
  include("tdaq_user_prefix-${arch}-${os}.cmake" OPTIONAL)
  include("tdaq_user_prefix-${cxx}.cmake" OPTIONAL)
  include("tdaq_user_prefix-${mode}.cmake" OPTIONAL)
  include("tdaq_user_prefix-${cxx}-${mode}.cmake" OPTIONAL)
  include("tdaq_user_prefix-${arch}-${os}-${cxx}.cmake" OPTIONAL)
  include("tdaq_user_prefix-${BINARY_TAG}.cmake" OPTIONAL)

  if(TDAQ_VARIANT)
    include("cmake/variants/${TDAQ_VARIANT}/prefix.cmake" OPTIONAL)
  endif()

  if(TDAQ_USER_PREFIX)
    include(${TDAQ_USER_PREFIX} OPTIONAL)
  endif()

  # If we are cross-compiling TDAQ release itself, use the same version for the native tools
  if(CMAKE_CROSSCOMPILING AND "${name}" STREQUAL "tdaq")
    _tdaq_version_to_name(TDAQ_NATIVE_PROJECT_VERSION ${name} ${version})
    set(TDAQ_NATIVE_PROJECT_VERSION ${TDAQ_NATIVE_PROJECT_VERSION} CACHE STRING "Native TDAQ version")
  endif()

  # This is used from the setup.sh.in scripts
  if(NOT "${PRJ_USES}" STREQUAL "")
    list(GET PRJ_USES 0 prj_name)
    list(GET PRJ_USES 1 prj_version)
    set(TDAQ_USED_PROJECT_NAME ${prj_name})
    _tdaq_version_to_name(TDAQ_USED_PROJECT_VERSION ${prj_name} ${prj_version})

    # IF this is a patch area, we need TDAQ_USED_PROJECT_NAME for
    # the RPM dependencies as above, but then we should just pretend that
    # we build against ourselves.
    if(TDAQ_PATCH_AREA)
      set(USES ${name} ${version})
      set(PRJ_USES ${name} ${version})
      set(PRJ_WORK TRUE)
    endif()

    while(PRJ_USES)
      list(GET PRJ_USES 0 prj_name)
      list(GET PRJ_USES 1 prj_version)
      message(STATUS "Using project: ${prj_name} ${prj_version}")
      _tdaq_use_project(${prj_name} ${prj_version})

      # If we are cross-compiling a project that depends on TDAQ, use the same
      # TDAQ version for the native tools
      if(CMAKE_CROSSCOMPILING AND "${prj_name}" STREQUAL "tdaq")
        _tdaq_version_to_name(TDAQ_NATIVE_PROJECT_VERSION ${prj_name} ${prj_version})
        set(TDAQ_NATIVE_PROJECT_VERSION ${TDAQ_NATIVE_PROJECT_VERSION} CACHE STRING "Native TDAQ version")
      endif()

      list(APPEND PROJECT_LIST ${prj_name})
      list(REMOVE_AT PRJ_USES 0 1)
    endwhile()
  endif()

  if(USE_CCACHE)
    find_program(CCACHE_EXECUTABLE NAMES ccache HINTS ${CCACHE_ROOT}/bin)
    if(CCACHE_EXECUTABLE)
      set(CMAKE_C_COMPILER_LAUNCHER   ${CCACHE_EXECUTABLE})
      set(CMAKE_CXX_COMPILER_LAUNCHER ${CCACHE_EXECUTABLE})
    endif()
  endif()

  if(CMAKE_VERSION VERSION_LESS 3.15.0)
    project(${name})
  endif()

  if("${CMAKE_GENERATOR}" STREQUAL "Ninja")
    enable_language(C CXX)
  else()
    enable_language(C CXX ${TDAQ_FORTRAN_LANGUAGE})
  endif()

  file(WRITE ${CMAKE_BINARY_DIR}/sw_repo.yaml "# Software Repository\n")

  set(TDAQ_IGUI_PROPERTIES_GLOBAL)
  set(TDAQ_IS_INFO_FILES_GLOBAL)

  tdaq_set_build_flags()

  set(TDAQ_RUNNER env CMTCONFIG=${BINARY_TAG} ${CMAKE_INSTALL_PREFIX}/share/bin/run_tdaq)

  enable_testing()

  set(TDAQ_PROJECT_NAME)
  _tdaq_version_to_name(TDAQ_PROJECT_NAME ${name} ${version})
  message(STATUS "Project: ${TDAQ_PROJECT_NAME}")

  if(DEFINED TDAQ_NATIVE_PROJECT_VERSION)
    message(STATUS "  Native TDAQ version: ${TDAQ_NATIVE_PROJECT_VERSION}")
    if(EXISTS /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/${TDAQ_NATIVE_PROJECT_VERSION}/installed)
      set(TDAQ_NATIVE_INST_PATH /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/${TDAQ_NATIVE_PROJECT_VERSION}/installed CACHE PATH "Native TDAQ location")
    elseif(EXISTS /cvmfs/atlas-online-nightlies.cern.ch/tdaq/nightlies/tdaq/${TDAQ_NATIVE_PROJECT_VERSION}/installed)
      set(TDAQ_NATIVE_INST_PATH /cvmfs/atlas-online-nightlies.cern.ch/tdaq/nightlies/tdaq/${TDAQ_NATIVE_PROJECT_VERSION}/installed CACHE PATH "Native TDAQ location")
    elseif(EXISTS /afs/cern.ch/atlas/project/tdaq/cmake/projects/tdaq/${TDAQ_NATIVE_PROJECT_VERSION}/installed)
      set(TDAQ_NATIVE_INST_PATH /afs/cern.ch/atlas/project/tdaq/cmake/projects/tdaq/${TDAQ_NATIVE_PROJECT_VERSION}/installed CACHE PATH "Native TDAQ location")
    elseif(EXISTS /sw/atlas/tdaq/${TDAQ_NATIVE_PROJECT_VERSION}/installed)
      set(TDAQ_NATIVE_INST_PATH /sw/atlas/tdaq/${TDAQ_NATIVE_PROJECT_VERSION}/installed CACHE PATH "Native TDAQ location")
    endif()

    set(TDAQ_NATIVE_TAG "x86_64-centos7-gcc8-opt" CACHE STRING "Native TDAQ configuration")

    message(STATUS "  Native TDAQ location: ${TDAQ_NATIVE_INST_PATH} Configuration: ${TDAQ_NATIVE_TAG}")
  endif()

  # CPACK general
  set(CPACK_PACKAGE_NAME "${name}")
  set(CPACK_PACKAGE_VENDOR "Atlas TDAQ")
  set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Atlas TDAQ Software")
  set(CPACK_PACKAGE_VERSION "${version}")
  set(CPACK_PACKAGE_VERSION_MAJOR "1")
  set(CPACK_PACKAGE_VERSION_MINOR "0")
  set(CPACK_PACKAGE_VERSION_PATCH "0")

  set(CPACK_RPM_PACKAGE_AUTOREQ NO)
  set(CPACK_RPM_PACKAGE_AUTOPROV NO)

  set(CPACK_PACKAGING_INSTALL_PREFIX "/sw/atlas/${name}/${TDAQ_PROJECT_NAME}/installed")
  set(CPACK_RPM_RELOCATION_PATHS "/sw/atlas")
  set(CPACK_RPM_NO_INSTALL_PREFIX_RELOCATION TRUE)

  set(CPACK_RPM_EXCLUDE_FROM_AUTO_FILELIST_ADDITION
    "${CPACK_PACKAGING_INSTALL_PREFIX}/include/rc"
    "${CPACK_PACKAGING_INSTALL_PREFIX}/include/test"
    "${CPACK_PACKAGING_INSTALL_PREFIX}/share/cmake"
    "${CPACK_PACKAGING_INSTALL_PREFIX}/share/data/daq/sw"
    "${CPACK_PACKAGING_INSTALL_PREFIX}/share/lib/python/omniidl_be"
    )

  set(CPACK_GENERATOR "RPM")
  set(CPACK_SOURCE_GENERATOR "RPM")
  set(CPACK_SOURCE_IGNORE_FILES "/installed/;/.svn/;/.git/")

  set(CPACK_PACKAGE_FILE_NAME "${TDAQ_PROJECT_NAME}")

  # RPM specific
  set(CPACK_RPM_COMPONENT_INSTALL ON)
  set(CPACK_RPM_PACKAGE_RELOCATABLE TRUE)
  set(CPACK_RPM_PACKAGE_GROUP "Atlas/TDAQ")

  if(EXISTS ${CMAKE_SOURCE_DIR}/cmake/template.spec.in)
    add_custom_command(OUTPUT template_project.spec template_noarch.spec template_src.spec
      COMMAND sed -e "s/@TDAQ_PACKAGE_NAME@_//" -e "s/@BINARY_TAG@/${BINARY_TAG}/" -e "s/@TDAQ_PACKAGE_VERSION@/${version}/" ${CMAKE_SOURCE_DIR}/cmake/template.spec.in > ${CMAKE_BINARY_DIR}/template_project.spec
      COMMAND sed -e "s/@TDAQ_PACKAGE_NAME@_//" -e "s/@BINARY_TAG@/noarch/" -e "s/@TDAQ_PACKAGE_VERSION@/${version}/" ${CMAKE_SOURCE_DIR}/cmake/template.spec.in > ${CMAKE_BINARY_DIR}/template_noarch.spec
      COMMAND sed -e "s/@TDAQ_PACKAGE_NAME@_//" -e "s/@BINARY_TAG@/src/" -e "s/@TDAQ_PACKAGE_VERSION@/${version}/" ${CMAKE_SOURCE_DIR}/cmake/template.spec.in > ${CMAKE_BINARY_DIR}/template_src.spec
      DEPENDS ${CMAKE_SOURCE_DIR}/cmake/template.spec.in)
     add_custom_target(${name}_spec ALL DEPENDS ${CMAKE_BINARY_DIR}/template_project.spec)

     add_custom_command(OUTPUT template_lcg.spec 
       COMMAND sed -e "s/@TDAQ_PACKAGE_NAME@/WithoutLCG/" -e "s/@BINARY_TAG@/${BINARY_TAG}/" -e "s/@TDAQ_PACKAGE_VERSION@/${version}/" ${CMAKE_SOURCE_DIR}/cmake/template.spec.in > ${CMAKE_BINARY_DIR}/template_lcg.spec
       DEPENDS ${CMAKE_SOURCE_DIR}/cmake/template.spec.in)
     add_custom_target(${name}_lcg_spec ALL DEPENDS ${CMAKE_BINARY_DIR}/template_lcg.spec)
  endif()

  string(REPLACE "-" "_" binary_tag ${BINARY_TAG})

  install(CODE "" COMPONENT WithoutLCG_${BINARY_TAG})
  install(CODE "" COMPONENT ${BINARY_TAG})
  set(CPACK_RPM_WithoutLCG_${BINARY_TAG}_USER_BINARY_SPECFILE ${CMAKE_BINARY_DIR}/template_lcg.spec)
  set(CPACK_RPM_WithoutLCG_${BINARY_TAG}_PACKAGE_REQUIRES "/bin/sh")
  set(CPACK_RPM_WithoutLCG_${BINARY_TAG}_PACKAGE_PROVIDES "/bin/sh")

  set(CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES "")
  
  # Add external LCG requirements
  if(TDAQ_SINGLE_RPM)
    set(CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES},LCG_${LCG_VERSION}_${binary_tag}")
  else()
  foreach(ext ${PRJ_EXTERNALS})
    string(TOUPPER ${ext} ext_upper)
    if(DEFINED ${ext_upper}_VERSION)
      string(REPLACE "-" "_" ext_version ${${ext_upper}_VERSION})
      if(TDAQ_HASHED_RPMS)
        set(CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES},${ext}-${${ext_upper}_HASH}_${ext_version}_${binary_tag}")
      else()
        set(CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES},LCG_${LCG_VERSION}_${ext}_${ext_version}_${binary_tag}")
      endif()
    else()
       if(NOT CMAKE_CROSSCOMPILING)
         message(WARNING "External ${ext} not available !")
       else()
         message(STATUS "External ${ext} not available - ignored for cross-compilation !")
       endif()
    endif()
  endforeach()
  endif()

  set(tdaq_name_version)
  set(PRJ_USES ${USES})
  while(PRJ_USES)
    list(GET PRJ_USES 0 prj_name)
    list(GET PRJ_USES 1 prj_version)
    if(NOT ${prj_name} STREQUAL "LCG")
      _tdaq_version_to_name(tdaq_name_version ${prj_name} ${prj_version})
      set(tdaq_name_version "${prj_name}_${tdaq_name_version}_${BINARY_TAG}")
      string(REPLACE "-" "_" underscored ${tdaq_name_version})
      set(CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES},${underscored}")

      _tdaq_version_to_name(tdaq_name_version ${prj_name} ${prj_version})
      set(tdaq_name_version "${tdaq_name_version}_WithoutLCG_${BINARY_TAG}")
      set(CPACK_RPM_WithoutLCG_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_WithoutLCG_${BINARY_TAG}_PACKAGE_REQUIRES},${tdaq_name_version}")
    endif()
    list(REMOVE_AT PRJ_USES 0 1)
  endwhile()
  
  if(NOT ENABLE_PACKAGES)
    set(ENABLE_PACKAGES "*")
  endif()

  FILE(GLOB children RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${ENABLE_PACKAGES})

  if(DISABLE_PACKAGES)
    list(REMOVE_ITEM children ${DISABLE_PACKAGES})
  endif()

  file(WRITE ${CMAKE_BINARY_DIR}/packages.txt "")

  SET(dirlist "")
  FOREACH(child ${children})
    IF(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${child})
      FILE(GLOB cmakelist RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${child}/CMakeLists.txt)
      IF(cmakelist)
         SET(dirlist ${dirlist} ${child})
      ENDIF()
    ENDIF()
  ENDFOREACH()

  set(TDAQ_ALL_TARGETS)

  set(TDAQ_ENABLED_PACKAGES ${dirlist})

  foreach(pkg ${dirlist})
    set(TDAQ_HAVE_${pkg} TRUE)
  endforeach()

  set(CPACK_RPM_${BINARY_TAG}_USER_BINARY_SPECFILE ${CMAKE_BINARY_DIR}/template_project.spec)

  if(NOT DEFINED TDAQ_DATA_URL)
    set(TDAQ_DATA_URL "http://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/data")
  endif()

  set(CPACK_RPM_src_PACKAGE_REQUIRES "/bin/sh")

  find_program(CTEST_COMMAND NAMES ctest)

  add_custom_target(test_pre
    COMMAND ${CTEST_COMMAND} -L pre_install)

  add_custom_target(test_post
    COMMAND ${CTEST_COMMAND} -L post_install)

  include_directories($<BUILD_INTERFACE:${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/include>)

  message(STATUS "Configuring packages:")
  foreach(pkg ${dirlist})
     add_subdirectory(${pkg})
     get_directory_property(dir_targets DIRECTORY ${pkg} DEFINITION TDAQ_TARGETS)

     get_directory_property(dir_igui_properties DIRECTORY ${pkg} DEFINITION TDAQ_IGUI_PROPERTIES)
     list(APPEND TDAQ_IGUI_PROPERTIES_GLOBAL ${dir_igui_properties})

     get_directory_property(dir_isinfo_files DIRECTORY ${pkg} DEFINITION TDAQ_IS_INFO_FILES)
     list(APPEND TDAQ_IS_INFO_FILES_GLOBAL ${dir_isinfo_files})

     add_custom_target(${pkg}_all)
     if(NOT "${dir_targets}" STREQUAL "")
       add_dependencies(${pkg}_all ${dir_targets})
     endif()

     set(TDAQ_ALL_TARGETS ${TDAQ_ALL_TARGETS} ${dir_targets})

     # Old code below, can probably be simplified now that dir_version is just the
     # project version.
     #
     # get_directory_property(dir_version DIRECTORY ${pkg} DEFINITION TDAQ_PACKAGE_VERSION)
     set(dir_version ${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION})
     string(REPLACE "-" "_" dir_version_underscored ${dir_version})

     add_custom_target(install_${pkg}
       COMMAND ${CMAKE_COMMAND} -DCOMPONENT="${pkg}_noarch-${dir_version}" -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_install.cmake
       COMMAND ${CMAKE_COMMAND} -DCOMPONENT="${pkg}_${BINARY_TAG}-${dir_version}" -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_install.cmake)
     if(NOT "${dir_targets}" STREQUAL "")
         add_dependencies(install_${pkg} ${dir_targets})
       endif()

     add_custom_target(install_${pkg}_fast
       COMMAND ${CMAKE_COMMAND} -DCOMPONENT="${pkg}_noarch-${dir_version}" -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_install.cmake
       COMMAND ${CMAKE_COMMAND} -DCOMPONENT="${pkg}_${BINARY_TAG}-${dir_version}" -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_install.cmake)

     set(CPACK_RPM_${pkg}_noarch-${dir_version}_USER_BINARY_SPECFILE ${CMAKE_BINARY_DIR}/${pkg}/template_noarch.spec)
     set(CPACK_RPM_${pkg}_src-${dir_version}_USER_BINARY_SPECFILE ${CMAKE_BINARY_DIR}/${pkg}/template_src.spec)
     set(CPACK_RPM_${pkg}_${BINARY_TAG}-${dir_version}_USER_BINARY_SPECFILE ${CMAKE_BINARY_DIR}/${pkg}/template_binary.spec)
     set(CPACK_RPM_${pkg}_noarch-${dir_version}_PACKAGE_ARCHITECTURE noarch)
     set(CPACK_RPM_${pkg}_src-${dir_version}_PACKAGE_ARCHITECTURE noarch)
     set(CPACK_RPM_${pkg}_${BINARY_TAG}-${dir_version}_PACKAGE_REQUIRES "${TDAQ_PROJECT_NAME}_${pkg}_noarch = ${dir_version_underscored}")
     set(CPACK_RPM_${pkg}_src-${dir_version}_PACKAGE_REQUIRES "${TDAQ_PROJECT_NAME}_${pkg}_noarch = ${dir_version_underscored}")
     set(CPACK_RPM_${pkg}_noarch-${dir_version}_PACKAGE_PROVIDES "/bin/sh")
     set(CPACK_RPM_${pkg}_src-${dir_version}_PACKAGE_PREFIX "/sw/atlas/${name}/${TDAQ_PROJECT_NAME}")

     # installation of sources into installed/.. 
     install(DIRECTORY ${CMAKE_SOURCE_DIR}/${pkg} COMPONENT ${pkg}_src-${dir_version} DESTINATION .. FILES_MATCHING PATTERN "*" PATTERN ".svn" EXCLUDE PATTERN ".git*" EXCLUDE)

     if(EXISTS ${CMAKE_BINARY_DIR}/${pkg}/post_install.sh)
       set(CPACK_RPM_${pkg}_${BINARY_TAG}-${dir_version}_POST_INSTALL_SCRIPT_FILE "${CMAKE_BINARY_DIR}/${pkg}/post_install.sh")
     endif()

     # set(CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES}\nRequires: ${TDAQ_PROJECT_NAME}_${pkg}_${BINARY_TAG}")
     set(CPACK_RPM_WithoutLCG_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_WithoutLCG_${BINARY_TAG}_PACKAGE_REQUIRES}\nRequires: ${TDAQ_PROJECT_NAME}_${pkg}_${BINARY_TAG}")
     set(CPACK_RPM_src_PACKAGE_REQUIRES "${CPACK_RPM_src_PACKAGE_REQUIRES}\nRequires: ${TDAQ_PROJECT_NAME}_${pkg}_src")

  endforeach()

  # project packages come through the WithoutLCG package
  set(CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES}\nRequires: ${TDAQ_PROJECT_NAME}_WithoutLCG_${BINARY_TAG}")

  set(CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_${BINARY_TAG}_PACKAGE_REQUIRES}\nRequires: ${TDAQ_PROJECT_NAME}_noarch")
  set(CPACK_RPM_WithoutLCG_${BINARY_TAG}_PACKAGE_REQUIRES "${CPACK_RPM_WithoutLCG_${BINARY_TAG}_PACKAGE_REQUIRES}\nRequires: ${TDAQ_PROJECT_NAME}_noarch")

  # the noarch package for the release, may be empty 
  set(provides ${name}_${TDAQ_PROJECT_NAME}_noarch)
  string(REPLACE "-" "_" CPACK_RPM_noarch_PACKAGE_PROVIDES ${provides})
  set(CPACK_RPM_noarch_PACKAGE_REQUIRES "/bin/sh")
  set(CPACK_RPM_noarch_PACKAGE_ARCHITECTURE noarch)
  set(CPACK_RPM_noarch_USER_BINARY_SPECFILE ${CMAKE_BINARY_DIR}/template_noarch.spec)

  set(CPACK_RPM_src_PACKAGE_ARCHITECTURE noarch)
  set(CPACK_RPM_src_USER_BINARY_SPECFILE ${CMAKE_BINARY_DIR}/template_src.spec)

  set(provides ${name}_${TDAQ_PROJECT_NAME}_${BINARY_TAG})
  string(REPLACE "-" "_" CPACK_RPM_${BINARY_TAG}_PACKAGE_PROVIDES ${provides})
  set(CPACK_RPM_${BINARY_TAG}_PACKAGE_PROVIDES "/bin/sh, ${CPACK_RPM_${BINARY_TAG}_PACKAGE_PROVIDES}")

  set(provides ${name}_${version}_${BINARY_TAG})
  string(REPLACE "-" "_" provides_ ${provides})
  set(CPACK_RPM_${BINARY_TAG}_PACKAGE_PROVIDES "${CPACK_RPM_${BINARY_TAG}_PACKAGE_PROVIDES}, ${provides_}")

  if(TDAQ_ALL_TARGETS)
    set(TARGET_EXPORT_DIR "${CMAKE_BINARY_DIR}/${CMAKE_FILES_DIRECTORY}/Export/share/cmake/${name}/${BINARY_TAG}")
    configure_file(${TDAQ_CMAKE_DIR}/cmake/scripts/fix_targets.cmake.in fix_targets.cmake @ONLY)
    install(SCRIPT ${CMAKE_BINARY_DIR}/fix_targets.cmake COMPONENT ${BINARY_TAG})
    install(EXPORT ${name} COMPONENT WithoutLCG_${BINARY_TAG} CONFIGURATIONS Release Debug DESTINATION share/cmake/${name}/${BINARY_TAG} NAMESPACE ${name}:: FILE ${name}Targets.cmake)
  endif()
  
  # DB relocation RPM post-install script
  if(EXISTS ${CMAKE_SOURCE_DIR}/post_install.sh.in)
    message(STATUS "Configuring post_install.sh")
    configure_file(post_install.sh.in post_install.sh @ONLY)
    set(CPACK_RPM_noarch_POST_INSTALL_SCRIPT_FILE "${CMAKE_BINARY_DIR}/post_install.sh")
  endif()

  if(EXISTS ${CMAKE_SOURCE_DIR}/post_install_binary.sh.in)
    # hack for now, should be unconditional
    if(TDAQ_HASHED_RPMS)
      message(STATUS "Configuring post_install_binary.sh")
      configure_file(post_install_binary.sh.in post_install_binary.sh @ONLY)
      set(CPACK_RPM_${BINARY_TAG}_POST_INSTALL_SCRIPT_FILE "${CMAKE_BINARY_DIR}/post_install_binary.sh")
    endif()
  endif()

  if(EXISTS "${CMAKE_SOURCE_DIR}/cmake/${name}.cmake.in")
    # message(STATUS "Creating config file for project ${name}")
    configure_package_config_file(cmake/${name}.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/${name}Config.cmake
                                  INSTALL_DESTINATION share/cmake/${name}/${BINARY_TAG}
                                  NO_SET_AND_CHECK_MACRO NO_CHECK_REQUIRED_COMPONENTS_MACRO)

    write_basic_package_version_file(${CMAKE_BINARY_DIR}/${name}ConfigVersion.cmake
                                    VERSION ${version}
                                    COMPATIBILITY ExactVersion )

    install(FILES ${CMAKE_BINARY_DIR}/${name}Config.cmake ${CMAKE_BINARY_DIR}/${name}ConfigVersion.cmake OPTIONAL COMPONENT WithoutLCG_${BINARY_TAG} DESTINATION share/cmake/${name}/${BINARY_TAG})
  endif()

  # Trigger generation of noarch project RPM
  install(CODE "" COMPONENT noarch)

  set(CPACK_RPM_src_PACKAGE_PREFIX "/sw/atlas/${name}/${TDAQ_PROJECT_NAME}")
  install(FILES CMakeLists.txt OPTIONAL COMPONENT src DESTINATION ..)

  if(IS_DIRECTORY ${CMAKE_SOURCE_DIR}/cmake)
    install(DIRECTORY ${CMAKE_SOURCE_DIR}/cmake OPTIONAL COMPONENT noarch DESTINATION share FILES_MATCHING PATTERN "*.cmake" PATTERN "*.in" PATTERN ".svn" EXCLUDE PATTERN ".git" EXCLUDE)
    install(DIRECTORY ${CMAKE_SOURCE_DIR}/cmake OPTIONAL COMPONENT src DESTINATION .. FILES_MATCHING PATTERN "*.cmake" PATTERN "*.in" PATTERN ".svn" EXCLUDE PATTERN ".git" EXCLUDE)
  endif()

  if(NOT PRJ_WORK AND EXISTS ${CMAKE_SOURCE_DIR}/cmake/last/CMakeLists.txt)
    message(STATUS "Adding custom post install package")
    add_subdirectory(cmake/last)
  endif()

  # message(STATUS "Targets: ${TDAQ_ALL_TARGETS}")

  # Now "import" all the exectuable and library targets of the immediate predecssor project
  # into this one. This is only done if the WORK flags is given to tdaq_project()
  if(PRJ_WORK)
    list(GET USES 0 prj_name)
    tdaq_import_targets(${prj_name} ${build_type})
    find_file(user_setup_in NAMES setup.user.in HINTS ${CMAKE_MODULE_PATH} PATH_SUFFIXES templates NO_DEFAULT_PATH)
    if(user_setup_in)
      configure_file(${user_setup_in} setup.sh @ONLY)
      install(FILES ${CMAKE_CURRENT_BINARY_DIR}/setup.sh OPTIONAL COMPONENT ${BINARY_TAG} DESTINATION .)
    else()
    endif()
  endif()

  foreach(imp ${PRJ_IMPORT})
     message(STATUS "    Importing targets from ${imp}")
     tdaq_import_targets(${imp} ${build_type})
  endforeach()

  install(PROGRAMS ${TDAQ_CMAKE_DIR}/cmake/scripts/run_tdaq OPTIONAL COMPONENT noarch DESTINATION share/bin)

  tdaq_sw_repo("IGUI_PROPERTIES:")
  foreach(prop ${TDAQ_IGUI_PROPERTIES_GLOBAL})
    tdaq_sw_repo("  - ${prop}")
  endforeach()

  tdaq_sw_repo("IS_INFO_FILES:")
  foreach(f ${TDAQ_IS_INFO_FILES_GLOBAL})
    tdaq_sw_repo("  - ${f}")
  endforeach()

  install(FILES ${CMAKE_BINARY_DIR}/sw_repo.yaml OPTIONAL COMPONENT noarch DESTINATION share/cmake)

  include(CPack)

  include("tdaq_user_postfix.cmake" OPTIONAL)
  include("tdaq_user_postfix-${arch}.cmake" OPTIONAL)
  include("tdaq_user_postfix-${os}.cmake" OPTIONAL)
  include("tdaq_user_postfix-${arch}-${os}.cmake" OPTIONAL)
  include("tdaq_user_postfix-${cxx}.cmake" OPTIONAL)
  include("tdaq_user_postfix-${mode}.cmake" OPTIONAL)
  include("tdaq_user_postfix-${cxx}-${mode}.cmake" OPTIONAL)
  include("tdaq_user_postfix-${arch}-${os}-${cxx}.cmake" OPTIONAL)
  include("tdaq_user_postfix-${BINARY_TAG}.cmake" OPTIONAL)

  if(TDAQ_VARIANT)
    include("cmake/variants/${TDAQ_VARIANT}/postfix.cmake" OPTIONAL)
  endif()

  if(TDAQ_USER_POSTFIX)
    include(${TDAQ_USER_POSTFIX} OPTIONAL)
  endif()

endfunction(tdaq_project)


# ######################################################################
# tdaq_add_python_package(name [SOURCE path])
#
# Add a python package 'name' to the installation.
# By default the source should be under ./python/<name>.
#
# If the Python version is >= 3, we first check if ./python3/<name> 
# exists. If yes, we use that, otherwise fall back to just 'python'.
#
# ######################################################################

function(tdaq_add_python_package name)
  cmake_parse_arguments(python_args "" "" "SOURCE" ${ARGN})

  # find_package(PythonInterp REQUIRED QUIET)

  if(NOT python_args_SOURCE)
    set(python_args_SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/python)
    if(${PYTHON_VERSION} GREATER_EQUAL 3.0.0)
      if(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/python3)
        set(python_args_SOURCE ${CMAKE_CURRENT_SOURCE_DIR}/python3)
      endif()
    endif()
  endif()
    
  install(DIRECTORY ${python_args_SOURCE}/${name} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/lib/python FILES_MATCHING PATTERN *.py PATTERN .git EXCLUDE)
endfunction()

function(tdaq_add_python_files)
  cmake_parse_arguments(ARGS "" "" "DESTINATION" ${ARGN})
  file(GLOB files ${ARGS_UNPARSED_ARGUMENTS})
  install(FILES ${files} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/lib/python/${ARGS_DESTINATION})
endfunction()

# ######################################################################
# tdaq_add_scripts([script1 script2... SHELL shell])
#
# Add binary independent scripts to the share/bin installation area.
#
# ######################################################################
function(tdaq_add_scripts)

  foreach(f ${ARGV})
    file(GLOB out ${f})
    install(PROGRAMS ${out} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/bin)
  endforeach()

endfunction()

# ######################################################################
# tdaq_idl_extra_options(FILE idl_file OPTIONS [...])
#
# ######################################################################
function(tdaq_idl_extra_options)
  cmake_parse_arguments(IDL "" "FILE" "OPTIONS" ${ARGN})
  set(${IDL_FILE}_EXTRA_OPTIONS "${IDL_OPTIONS}" CACHE INTERNAL "Extra IDL compilation flags for ${IDL_FILE} file")
endfunction()

# ######################################################################
# tdaq_add_idl(idl_source [DEPENDS ...][INCLUDE_DIRECTORIES ...] [CPP_OUTPUT var])
#
# Generate c++ code from IDL.
#
# ######################################################################
function(tdaq_add_idl idl_file)

  # We need this at build time for the correct environment
  find_package(PythonInterp REQUIRED QUIET)
  find_package(PythonLibs REQUIRED QUIET)
  get_filename_component(python_libdir ${PYTHON_LIBRARIES} DIRECTORY)

  if(${PYTHON_VERSION} VERSION_GREATER_EQUAL 3.0.0)
    set(suffix "3")
  endif()

  cmake_parse_arguments(idl_gen "" "CPP_OUTPUT;INCLUDE_OUTPUT" "DEPENDS;INCLUDE_DIRECTORIES" ${ARGN})

  set(OMNIIDL_OPTIONS -bcxx -Wbtp -Wbkeep_inc_path ${${idl_file}_EXTRA_OPTIONS})
  set(OMNIIDL_DEPENDS omnicpp)

  # Choose the proper generator, based on
  # - are we cross-compiling -> use host system tool
  # - is omni in work area -> use local version
  # - else use tool from release area
  if(CMAKE_CROSSCOMPILING)
    set(OMNIIDL_CMD ${TDAQ_CMAKE_DIR}/cmake/scripts/omniidl.sh ${TDAQ_NATIVE_INST_PATH} ${TDAQ_NATIVE_TAG})

  elseif(DEFINED TDAQ_HAVE_omni)
    set(OMNIIDL_CMD  env PATH=${CMAKE_BINARY_DIR}/omni:$ENV{PATH} PYTHONPATH=${CMAKE_BINARY_DIR}/omni:${CMAKE_SOURCE_DIR}/omni/src/lib/omniORB/python${suffix}:${CMAKE_SOURCE_DIR}/omni/src/tool/omniidl/python${suffix}:$ENV{PYTHONPATH} LD_LIBRARY_PATH=${CMAKE_BINARY_DIR}/omni:$ENV{LD_LIBRARY_PATH} ${PYTHON_EXECUTABLE} ${CMAKE_SOURCE_DIR}/omni/src/tool/omniidl/python${suffix}/scripts/omniidlrun.py)
  else()
    find_package(PythonInterp)
    find_program(OMNIIDL NAMES omniidl HINTS $ENV{TDAQ_INST_PATH}/share/bin)
    set(OMNIIDL_CMD env PATH=${TDAQ_INST_PATH}/share/bin:${TDAQ_INST_PATH}/${BINARY_TAG}/bin:$ENV{PATH} LD_LIBRARY_PATH=${TDAQ_INST_PATH}/${BINARY_TAG}/lib:${python_libdir}:$ENV{LD_LIBRARY_PATH} PYTHONPATH=${TDAQ_INST_PATH}/share/lib/python:${TDAQ_INST_PATH}/${BINARY_TAG}/lib:$ENV{PYTHONPATH} ${PYTHON_EXECUTABLE} ${OMNIIDL})
    unset(OMNIIDL_DEPENDS)
  endif()

  set(base)
  get_filename_component(base ${idl_file} NAME_WE)
  # message(STATUS "    IDL => C++ for ${idl_file}")

  set(includes -I${CMAKE_BINARY_DIR}/installed/include)
  foreach(inc ${idl_gen_INCLUDE_DIRECTORIES})
    if(DEFINED TDAQ_HAVE_${inc})
      set(includes ${includes} $<BUILD_INTERFACE:-I${inc}>)
    endif()
  endforeach()
  set(includes ${includes} -I${TDAQ_INST_PATH}/include)

  set(depends)
  foreach(d ${idl_gen_DEPENDS})
    if(DEFINED TDAQ_HAVE_${d} OR TARGET IDL_${d})
      set(depends ${depends} IDL_${d})
    endif()
  endforeach()

  file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/idl/${base})

  add_custom_command(OUTPUT idl/${base}/${base}.hh idl/${base}/${base}SK.cc idl/${base}/${base}DynSK.cc
    COMMAND ${OMNIIDL_CMD} ${OMNIIDL_OPTIONS} ${includes} -I${CMAKE_CURRENT_SOURCE_DIR}/idl -I${CMAKE_CURRENT_SOURCE_DIR}/idl/CosNaming ${CMAKE_CURRENT_SOURCE_DIR}/${idl_file}
    COMMAND cp -f ${base}.hh idl/${base}/${base}.hh
    COMMAND cp -f ${base}SK.cc idl/${base}/${base}SK.cc
    COMMAND touch ${base}DynSK.cc && cp -f ${base}DynSK.cc idl/${base}/${base}DynSK.cc
    COMMAND mkdir -p ${CMAKE_BINARY_DIR}/installed/include/${base}/
    COMMAND cp -f ${CMAKE_CURRENT_SOURCE_DIR}/${idl_file} ${CMAKE_BINARY_DIR}/installed/include/${base}
    DEPENDS ${idl_file} ${depends} ${OMNIIDL_DEPENDS}
    )
  
  add_custom_target(IDL_${base} DEPENDS idl/${base}/${base}.hh idl/${base}/${base}SK.cc idl/${base}/${base}DynSK.cc)

  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/idl/${base}/${base}.hh OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION include/${base})
  install(FILES  ${idl_file} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION include/${base})

  if(idl_gen_CPP_OUTPUT)
    set(${idl_gen_CPP_OUTPUT} idl/${base}/${base}.hh idl/${base}/${base}SK.cc idl/${base}/${base}DynSK.cc PARENT_SCOPE)
  endif()

  if(idl_gen_INCLUDE_OUTPUT)
    set(${idl_gen_INCLUDE_OUTPUT} $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/idl>  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/idl> PARENT_SCOPE)
  endif()
  
endfunction(tdaq_add_idl)

########################################################################
# tdaq_add_idl_java(idl_file [JAVA_OUTPUT srcs] [ARGS ...])
########################################################################

function(tdaq_add_idl_java idl_file)
     cmake_parse_arguments(idl_java "" "JAVA_OUTPUT" "IDL_DEPENDS;IDL_ARGS" ${ARGN})

     get_filename_component(name ${idl_file} NAME_WE)
     # message(STATUS "    IDL => Java for ${idl_file}")

     file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/java/${name})

     set(includes)
     foreach(dep ${idl_java_IDL_DEPENDS})
       if(DEFINED TDAQ_HAVE_${dep})
         set(includes ${includes} -I${CMAKE_SOURCE_DIR}/${dep}/idl)
       endif()
     endforeach()
     set(includes ${includes} -I${TDAQ_INST_PATH}/include)

     # new version

     set(depends)
     if(CMAKE_CROSSCOMPILING)
       set(TDAQ_JAVA_EXTERNALS -cp ${TDAQ_NATIVE_INST_PATH}/share/lib/external.jar)
     elseif(DEFINED TDAQ_HAVE_TDAQExtJars)
       set(depends JAR_TDAQExtJars_external)
       set(TDAQ_JAVA_EXTERNALS -cp ${CMAKE_BINARY_DIR}/TDAQExtJars/external.jar)
     else()
       set(TDAQ_JAVA_EXTERNALS -cp ${TDAQ_INST_PATH}/share/lib/external.jar)
       # CLASSPATH is set by tdaqConfig.cmake
     endif()

     add_custom_command(OUTPUT java/${name}/filelist.txt
       COMMAND ${Java_JAVA_EXECUTABLE} ${TDAQ_JAVA_EXTERNALS} org.jacorb.idl.parser  ${idl_java_IDL_ARGS} -Iidl ${includes} -d ${CMAKE_CURRENT_BINARY_DIR}/java/${name} ${idl_file}
       COMMAND find ${CMAKE_CURRENT_BINARY_DIR}/java/${name} -name *.java > ${CMAKE_CURRENT_BINARY_DIR}/java/${name}/filelist.txt
       WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
       DEPENDS ${depends} ${idl_file}
       COMMENT "Generating Java files for ${name}.idl"
       VERBATIM
       )
     
     # old version, generate at configure time
     
     # execute_process(
     #   COMMAND ${Java_JAVA_EXECUTABLE} -cp ${TDAQ_JAVA_EXTERNALS} org.jacorb.idl.parser  ${idl_java_IDL_ARGS} -Iidl ${includes} -d ${CMAKE_CURRENT_BINARY_DIR}/java/${name} ${idl_file}
     #   WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
     #   )

     # set(srcs)
     # execute_process(
     #   COMMAND find ${CMAKE_CURRENT_BINARY_DIR}/java/${name} -name *.java
     #   COMMAND tr "\\n" ";"
     #   OUTPUT_VARIABLE srcs)

     if(idl_java_JAVA_OUTPUT)
       set(${idl_java_JAVA_OUTPUT} @${CMAKE_CURRENT_BINARY_DIR}/java/${name}/filelist.txt  PARENT_SCOPE)
     endif()

endfunction()

# ######################################################################
# tdaq_generate_dal(sources... 
#                      NAMESPACE ns
#                      [TARGET target]
#                      [INCLUDE_DIRECTORIES ...] 
#                      [CLASSES ...] 
#                      [PACKAGE name] 
#                      [INCLUDE dir] 
#                      [NOINSTALL]
#                      [CPP dir] [JAVA dir] 
#                      [CPP_OUTPUT var1] [JAVA_OUTPUT jvar2]
#                      [DUMP_OUTPUT var2])
# ######################################################################
function(tdaq_generate_dal)

   cmake_parse_arguments(config_opts "NOINSTALL" "TARGET;PACKAGE;NAMESPACE;CPP;JAVA;INCLUDE;CPP_OUTPUT;JAVA_OUTPUT;DUMP_OUTPUT" "INCLUDE_DIRECTORIES;CLASSES" ${ARGN})
   set(srcs ${config_opts_UNPARSED_ARGUMENTS})

   if(NOT config_opts_TARGET)
     set(config_opts_TARGET DAL_${TDAQ_PACKAGE_NAME})
   endif()

   list(APPEND TDAQ_GENCONFIG_INCLUDES ${CMAKE_CURRENT_BINARY_DIR}/genconfig_${config_opts_TARGET})
   set(TDAQ_GENCONFIG_INCLUDES ${TDAQ_GENCONFIG_INCLUDES} PARENT_SCOPE)

   if(config_opts_CLASSES)
     set(class_option -c ${config_opts_CLASSES})
   endif()

   set(package ${TDAQ_PACKAGE_NAME})
   if(config_opts_PACKAGE)
      set(package ${config_opts_PACKAGE})
   endif()

   set(cpp_dir ${config_opts_TARGET}.tmp.cpp)
   if(config_opts_CPP)
      set(cpp_dir ${config_opts_CPP})
   endif()

   set(java_dir tmp.java)
   if(config_opts_JAVA)
      set(java_dir ${config_opts_JAVA})
   endif()
   set(java_dir ${java_dir}/${package})

   if(NOT config_opts_NAMESPACE)
      message(ERROR "NAMESPACE option is required")
   endif()

   string(REPLACE "::" "__" dump_suffix ${config_opts_NAMESPACE})
   if(config_opts_DUMP_OUTPUT)
     set(dump_srcs ${cpp_dir}/dump/dump_${dump_suffix}.cpp)
   endif()

   set(hpp_dir)
   if(config_opts_INCLUDE)
      set(hpp_dir ${config_opts_INCLUDE})
   else()
      string(REPLACE "::" "/" hpp_dir ${config_opts_NAMESPACE})
   endif()

   set(config_dependencies)

   if(TDAQ_USED_PROJECT_NAME)
     set(project_db_area ${${TDAQ_USED_PROJECT_NAME}_DIR})
     get_filename_component(project_db_area ${project_db_area} DIRECTORY)
     get_filename_component(project_db_area ${project_db_area} DIRECTORY)
     get_filename_component(project_db_area ${project_db_area} DIRECTORY)
     set(project_db_area "${project_db_area}/data")
   endif()

   if(TDAQ_GENCONFIG_INCLUDES OR config_opts_INCLUDE_DIRECTORIES)
     set(config_includes -I ${TDAQ_GENCONFIG_INCLUDES})
     if(config_opts_INCLUDE_DIRECTORIES)
       foreach(inc ${config_opts_INCLUDE_DIRECTORIES})
         if(DEFINED TDAQ_HAVE_${inc})
           # if in same build area...
           list(APPEND config_includes ${CMAKE_BINARY_DIR}/${inc})
           list(APPEND config_dependencies DAL_${inc})
         else()
           list(APPEND config_includes ${TDAQ_INST_PATH}/share/data/${inc} ${project_db_area}/${inc})
         endif()
       endforeach()
     endif()
   endif()
   
   file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/installed/share/data/${TDAQ_DB_PROJECT}/schema)
   
   set(schemas)
   foreach(src ${srcs})
     set(schemas ${schemas} ${CMAKE_CURRENT_SOURCE_DIR}/${src})
     file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/${src} DESTINATION ${CMAKE_BINARY_DIR}/installed/share/data/${TDAQ_DB_PROJECT}/schema)
   endforeach()
   
   foreach(schema ${schemas}) 

     execute_process(
       COMMAND grep "[ \t]*<class name=\"" ${schema}
       COMMAND sed "s;[ \t]*<class name=\";;"
       COMMAND sed s:\".*::
       COMMAND tr "\\n" " "
       OUTPUT_VARIABLE class_out 
       )

     separate_arguments(class_out)

     if(config_opts_CLASSES)
       set(out)
       foreach(cand ${class_out})
         list(FIND config_opts_CLASSES ${cand} found)
         if(NOT ${found} EQUAL -1)
           set(out ${out} ${cand})
         endif()
       endforeach()
       set(class_out ${out})
     endif()

     foreach(s ${class_out})
       set(cpp_source ${cpp_source} ${cpp_dir}/${s}.cpp ${hpp_dir}/${s}.h)
       set(java_source ${java_source} ${CMAKE_CURRENT_BINARY_DIR}/${java_dir}/${s}_Helper.java ${CMAKE_CURRENT_BINARY_DIR}/${java_dir}/${s}_Impl.java ${CMAKE_CURRENT_BINARY_DIR}/${java_dir}/${s}.java)
     endforeach()

 endforeach()
   
   separate_arguments(cpp_source)
   separate_arguments(java_source)

   set(GENCONFIG_DEPENDS)
   if(CMAKE_CROSSCOMPILING)
     set(GENCONFIG_BINARY ${TDAQ_CMAKE_DIR}/cmake/scripts/genconfig.sh ${TDAQ_NATIVE_INST_PATH} ${TDAQ_NATIVE_TAG} ${CMAKE_BINARY_DIR}/installed/share/data:${TDAQ_NATIVE_INST_PATH}/share/data)
   elseif(DEFINED TDAQ_HAVE_genconfig)
     set(GENCONFIG_DEPENDS genconfig)
     set(GENCONFIG_BINARY env TDAQ_DB_PATH=${CMAKE_BINARY_DIR}/installed/share/data ${CMAKE_BINARY_DIR}/genconfig/genconfig)
   else()
     set(GENCONFIG_BINARY env TDAQ_DB_PATH=${CMAKE_BINARY_DIR}/installed/share/data:${TDAQ_INST_PATH}/share/data:${project_db_area} PATH=${TDAQ_INST_PATH}/${BINARY_TAG}/bin:$ENV{PATH} LD_LIBRARY_PATH=${TDAQ_INST_PATH}/external/${BINARY_TAG}/lib:${TDAQ_INST_PATH}/${BINARY_TAG}/lib:${TDAQC_INST_PATH}/${BINARY_TAG}/lib:${TDAQC_INST_PATH}/external/${BINARY_TAG}/lib:$ENV{LD_LIBRARY_PATH} genconfig -I ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_INSTALL_PREFIX}/share/data ${TDAQ_INST_PATH}/share/data ${project_db_area})
   endif()

   set(tmp_target MKTMP_${config_opts_TARGET})
   if(TARGET ${tmp_target})
     message(SEND_ERROR "You are using more than one tdaq_generate_dal() command inside this package. Please use the TARGET <name> argument to distinguish them")
   endif()

   add_custom_target(${tmp_target}
     COMMAND mkdir -p ${cpp_dir} ${cpp_dir}/dump ${hpp_dir} ${java_dir} genconfig_${config_opts_TARGET})
   
   add_custom_command(
     OUTPUT genconfig_${config_opts_TARGET}/genconfig.info ${cpp_source} ${java_source} ${dump_srcs}
     COMMAND ${GENCONFIG_BINARY} -i ${hpp_dir} -n ${config_opts_NAMESPACE} -d ${cpp_dir}  -j ${java_dir} -p ${package} ${class_option} ${config_includes} -s ${schemas}
     COMMAND cp -f ${cpp_dir}/*.h ${hpp_dir}/
     COMMAND cp -f ${cpp_dir}/dump*.cpp ${cpp_dir}/dump
     COMMAND cp genconfig.info genconfig_${config_opts_TARGET}/
     DEPENDS ${schemas} ${config_dependencies} ${GENCONFIG_DEPENDS} ${tmp_target})

   add_custom_target(${config_opts_TARGET} ALL DEPENDS ${cpp_source} ${java_source})

   if(config_opts_CPP_OUTPUT)
     set(${config_opts_CPP_OUTPUT} ${cpp_source} PARENT_SCOPE)
   endif()

   if(config_opts_JAVA_OUTPUT)
     set(${config_opts_JAVA_OUTPUT} ${java_source} PARENT_SCOPE)
   endif()

   if(config_opts_DUMP_OUTPUT)
     set(${config_opts_DUMP_OUTPUT} ${dump_srcs} PARENT_SCOPE)
   endif()

   if(NOT config_opts_NOINSTALL)
     if(config_opts_CPP_OUTPUT)
       install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${hpp_dir} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION include FILES_MATCHING PATTERN *.h)
     endif()
   endif()

   # Always install genconfig.info files, independent of NOINSTALL option
   install(FILES ${CMAKE_CURRENT_BINARY_DIR}/genconfig_${config_opts_TARGET}/genconfig.info OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/data/${TDAQ_PACKAGE_NAME})

endfunction()

# ######################################################################
#
# tdaq_generate_isinfo(target_name
#                      [CPP_OUTPUT var]
#                      [JAVA_OUTPUT var]
#                      [OUTPUT_DIRECTORY dir]
#                      [NAMED][SIMPLE]
#                      [NAMESPACE names]
#                      [PACKAGE name]
#                      [PREFIX dir]
#                      [NO_NAMESPACE]
#                      [NO_PRINT]
#                      [NOINSTALL]
#                      [CLASSNAME class]
#                      [INPUTS file..]
#                      [ARRAY] schema_xml_file...)
# 
# ######################################################################
function(tdaq_generate_isinfo target_name)
  cmake_parse_arguments(ARG "NAMED;SIMPLE;NO_NAMESPACE;NO_PRINT;ARRAY;NOINSTALL" "CPP_OUTPUT;JAVA_OUTPUT;OUTPUT_DIRECTORY;PACKAGE;NAMESPACE;PREFIX;CLASSNAME" "INPUTS" ${ARGN})

  set(args)
  if(ARG_JAVA_OUTPUT)
    set(args ${args} --java)
  endif()

  if(ARG_CPP_OUTPUT)
    set(args ${args} --cpp)
  endif()

  if(ARG_NAMED)
    set(args ${args} --named)
  elseif(NOT ARG_SIMPLE)
    set(ARG_SIMPLE TRUE)
  endif()

  if(ARG_SIMPLE)
    set(args ${args} --simple)
  endif()

  if(ARG_NAMESPACE)
    string(REPLACE "::" " --package " ns_args ${ARG_NAMESPACE})
    set(args ${args} --package ${ns_args})
  endif()

  if(ARG_PACKAGE)
    string(REPLACE "." " --package " p_args ${ARG_PACKAGE})
    set(args ${args} --package ${p_args})
  endif()

  if(ARG_NO_NAMESPACE)
    set(args ${args} --no-namespace)
  endif()

  if(ARG_NO_PRINT)
    set(args ${args} --no-print)
  endif()
    
  if(ARG_ARRAY)
    set(args ${args} --array)
  endif()

  if(NOT ARG_OUTPUT_DIRECTORY)
    set(ARG_OUTPUT_DIRECTORY gen_is)
  endif()
  set(args ${args} --outdir ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY})
  file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY})

  if(ARG_PREFIX)
    set(args ${args} --prefix ${ARG_PREFIX})
  endif()

  if(ARG_CLASSNAME)
    set(args ${args} --class-name ${ARG_CLASSNAME})
  endif()

  set(cpp_srcs)
  set(java_srcs)

  foreach(schema_file ${ARG_UNPARSED_ARGUMENTS})

    set(class_names)
    if(ARG_CLASSNAME)
      set(class_names ${ARG_CLASSNAME})
    else()
      execute_process(
        COMMAND grep "[ \t]*<class name=\"" ${schema_file}
        COMMAND sed "s;[ \t]*<class name=\";;"
        COMMAND sed s:\".*::
        COMMAND tr "\\n" ";"
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        OUTPUT_VARIABLE class_names
        )
    endif()

    foreach(class ${class_names})

      if(ARG_NAMED)
        if(ARG_CPP_OUTPUT)
          set(cpp_srcs ${cpp_srcs} ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY}/${class}Named.h)
        endif()
        
        if(ARG_JAVA_OUTPUT)
          set(java_srcs ${java_srcs} ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY}/${class}Named.java)
        endif()
      endif()
      
      if(ARG_SIMPLE)
        if(ARG_CPP_OUTPUT)
          set(cpp_srcs ${cpp_srcs} ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY}/${class}.h)
        endif()
        
        if(ARG_JAVA_OUTPUT)
          set(java_srcs ${java_srcs} ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY}/${class}.java)
        endif()
      endif()
      
    endforeach()

  endforeach()

  # do the thing...

  separate_arguments(args)

  set(more_depends)
  # local is*.jar  and script ?
  if(DEFINED TDAQ_HAVE_is)
    set(is_cp ${is_cp}:${CMAKE_BINARY_DIR}/is/is_generator.jar:${CMAKE_BINARY_DIR}/is/is_util.jar:${CMAKE_BINARY_DIR}/is/is.jar)
    set(more_depends JAR_is_is_generator JAR_is_is_util JAR_is_is)
    set(is_env PATH=${CMAKE_SOURCE_DIR}/is/scripts:$ENV{PATH})
  else()
    set(is_env PATH=${TDAQ_INST_PATH}/share/bin:$ENV{PATH})
  endif()

  # local ipc.jar ?
  if(DEFINED TDAQ_HAVE_ipc)
    set(is_cp ${is_cp}:${CMAKE_BINARY_DIR}/ipc/ipc.jar)
    set(more_depends ${more_depends} JAR_ipc_ipc)
  endif()

  # local external.jar ?
  if(CMAKE_CROSSCOMPILING)
    set(is_cp ${is_cp}:${TDAQ_NATIVE_INST_PATH}/share/lib/external.jar)
  elseif(DEFINED TDAQ_HAVE_TDAQExtJars)
    set(is_cp ${is_cp}:${CMAKE_BINARY_DIR}/TDAQExtJars/external.jar)
    set(more_depends ${more_depends} JAR_TDAQExtJars_external)
  endif()

  set(classpath)
  foreach(p ${TDAQ_CLASSPATH})
    set(classpath "${classpath}:${p}")
  endforeach()

  # Add explicit input files that have user code
  set(more_inputs)
  set(inputs)
  foreach(input ${ARG_INPUTS})
    list(APPEND more_inputs ${CMAKE_CURRENT_SOURCE_DIR}/${input})
    list(APPEND inputs ${CMAKE_CURRENT_BINARY_DIR}/${input})
  endforeach()

  if(more_inputs)
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${target_name}_inputs.stamp
      COMMAND mkdir -p ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY}
      COMMAND ${CMAKE_COMMAND} -E copy_if_different ${more_inputs} ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY}
      COMMAND touch ${CMAKE_CURRENT_BINARY_DIR}/${target_name}_inputs.stamp
      DEPENDS ${more_inputs}
      WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
      COMMENT "Copying inputs: ${more_inputs}"
    )
  else()
    add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${target_name}_inputs.stamp
      COMMAND touch ${CMAKE_CURRENT_BINARY_DIR}/${target_name}_inputs.stamp
      )
  endif()

  # message(STATUS "    IS Info: ${ARG_UNPARSED_ARGUMENTS}")
  add_custom_command(OUTPUT ${cpp_srcs} ${java_srcs}
    COMMAND env JAVA_HOME=${JAVA_ROOT} CLASSPATH=${is_cp}:${classpath} ${is_env} is_generator.sh ${args} ${ARG_UNPARSED_ARGUMENTS}
    DEPENDS ${ARG_UNPARSED_ARGUMENTS} ${more_depends} ${CMAKE_CURRENT_BINARY_DIR}/${target_name}_inputs.stamp
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "Generating IS classes for: ${ARG_UNPARSED_ARGUMENTS}"
    )

  if(ARG_CPP_OUTPUT)
    set(${ARG_CPP_OUTPUT} ${cpp_srcs} PARENT_SCOPE)

    add_library(${target_name} INTERFACE)
    target_include_directories(${target_name} INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>) 
    target_include_directories(${target_name} INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY}>)

    add_custom_target(ISINFO_${target_name} ALL DEPENDS ${cpp_srcs})
    add_dependencies(${target_name} ISINFO_${target_name})

    install(TARGETS ${target_name} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} EXPORT ${PROJECT_NAME})
  endif()

  if(ARG_JAVA_OUTPUT)
    set(${ARG_JAVA_OUTPUT} ${java_srcs} PARENT_SCOPE)
  endif()

  if(NOT ARG_NOINSTALL)
    if(ARG_CPP_OUTPUT)
      install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${ARG_OUTPUT_DIRECTORY} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION include FILES_MATCHING PATTERN *.h)
      set(TDAQ_TARGETS ${TDAQ_TARGETS} ${target_name} PARENT_SCOPE)
    endif()
  endif()
  
endfunction()

# A copy of CMake's install_jar with the OPTIONAL flag added
function(tdaq_install_jar _TARGET_NAME)
    if (ARGC EQUAL 2)
      set (_DESTINATION ${ARGV1})
    else()
      cmake_parse_arguments(_install_jar
        ""
        "DESTINATION;COMPONENT"
        ""
        ${ARGN})
      if (_install_jar_DESTINATION)
        set (_DESTINATION ${_install_jar_DESTINATION})
      else()
        message(SEND_ERROR "install_jar: ${_TARGET_NAME}: DESTINATION must be specified.")
      endif()

      if (_install_jar_COMPONENT)
        set (_COMPONENT COMPONENT ${_install_jar_COMPONENT})
      endif()
    endif()

    get_property(__FILES
        TARGET
            ${_TARGET_NAME}
        PROPERTY
            INSTALL_FILES
    )
    set_property(
        TARGET
            ${_TARGET_NAME}
        PROPERTY
            INSTALL_DESTINATION
            ${_DESTINATION}
    )

    if (__FILES)
        install(
            FILES
                ${__FILES}
            OPTIONAL
            DESTINATION
                ${_DESTINATION}
            ${_COMPONENT}
        )
    else ()
        message(SEND_ERROR "install_jar: The target ${_TARGET_NAME} is not known in this scope.")
    endif ()
endfunction()


#######################################################################
#
# tdaq_add_jar(jar 
#              source1 [source2...] [resource1...]
#              [DAL]
#              [NOINSTALL]
#              [INCLUDE_JARS jar1 [jar2...] ]
#              [ENTRY_POINT entry]
#              [VERSION version]
#              [OUTPUT_NAME name]
#              [OUTPUT_DIR dir]
#              [MANIFEST manifest]
#              [SOURCES src1 src2...]
#              [IDL_DEPENDS ...]
#              [IDL_ARGS ...]
#              [ADD_TO_OKS]          - add binary to OKS software repository
#              [DESCRIPTION ...]     - add description to binary in OKS software repository
#                 
# Wrapper around add_jar() supporting globbing and CORBA IDL.
# 
#######################################################################
function(tdaq_add_jar target)

  list(FIND TDAQ_DISABLED_TARGETS ${name} _disabled)
  if(NOT ${_disabled} EQUAL -1)
    return()
  endif()

  cmake_parse_arguments(ARG "NOINSTALL;DAL;ADD_TO_OKS" "ENTRY_POINT;VERSION;OUTPUT_NAME;OUTPUT_DIR;MANIFEST;DESCRIPTION;GENERATE_NATIVE_HEADERS;DESTINATION" "SOURCES;IDL_DEPENDS;IDL_ARGS;INCLUDE_JARS" ${ARGN})

  if(NOT Java_DIR)
    tdaq_use_java()
  endif()

  set(args ${target})

  set(srcs)
  set(deps)
  foreach(f ${ARG_SOURCES} ${ARG_UNPARSED_ARGUMENTS})
    if(${f} MATCHES ".+\\.idl")
      set(idl_srcs)
      tdaq_add_idl_java(${f} IDL_DEPENDS ${ARG_IDL_DEPENDS} IDL_ARGS ${ARG_IDL_ARGS} JAVA_OUTPUT idl_srcs)
      list(APPEND srcs ${idl_srcs})
      get_filename_component(idl_base ${f} NAME_WE)
      list(APPEND deps IDL_${idl_base})
    elseif(${f} MATCHES ".*\\*.*")
      # try to glob
      set(out)
      file(GLOB out LIST_DIRECTORIES false RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${f})
      set(srcs ${srcs} ${out})
    else()
      # may be generated file, so just add
      set(srcs ${srcs} ${f})
    endif()
  endforeach()

  set(args ${args} SOURCES ${srcs})
  
  if(ARG_ENTRY_POINT)
    set(args ${args} ENTRY_POINT ${ARG_ENTRY_POINT})
  endif()

  if(ARG_VERSION)
    set(args ${args} VERSION ${ARG_VERSION})    
  endif()

  if(ARG_OUTPUT_NAME)
    set(args ${args} OUTPUT_NAME ${ARG_OUTPUT_NAME})    
  endif()

  if(ARG_OUTPUT_DIR)
    set(args ${args} OUTPUT_DIR ${ARG_OUTPUT_DIR})
  endif()

  if(ARG_MANIFEST)
    set(args ${args} MANIFEST ${ARG_MANIFEST})
  endif()

  if(ARG_GENERATE_NATIVE_HEADERS)
    set(args ${args} GENERATE_NATIVE_HEADERS ${ARG_GENERATE_NATIVE_HEADERS})
    if(ARG_DESTINATION)
       set(args ${args} DESTINATION ${ARG_DESTINATION})
    endif()
  endif()

  if(ARG_INCLUDE_JARS)
    set(jars)
    foreach(jar ${ARG_INCLUDE_JARS})
      if(TARGET ${jar} OR EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${jar})
        list(APPEND jars ${jar})
        if(TARGET ${jar})
           list(APPEND deps ${jar})
        endif()
      else()
        get_filename_component(dir  ${jar} DIRECTORY)
        get_filename_component(base ${jar} NAME)
        if(CMAKE_CROSSCOMPILING AND EXISTS ${TDAQ_NATIVE_INST_PATH}/share/lib/${base})
          list(APPEND jars ${TDAQ_NATIVE_INST_PATH}/share/lib/${base})
        else()
          string(REGEX REPLACE ".jar$" "" base ${base})
          if((NOT "${dir}" STREQUAL "") AND DEFINED TDAQ_HAVE_${dir})
            # Form is: package/name.jar which should be created
            # in that package's binary directory; a target of a well-defined
            # name should exist: JAR_${package}_{base}
            # message(STATUS "    Adding jar: ${CMAKE_BINARY_DIR}/${jar}")
            list(APPEND jars ${CMAKE_BINARY_DIR}/${jar})
            list(APPEND deps JAR_${dir}_${base})

            get_filename_component(full_path_jar ${TDAQ_INST_PATH}/share/lib/${base}.jar ABSOLUTE)
            # message(STATUS "to remove from TDAQ_CLASSPATH: ${full_path_jar}")
	    list(REMOVE_ITEM TDAQ_CLASSPATH "${full_path_jar}")
          elseif(EXISTS ${jar})
            # message(STATUS "   WARNING: Adding jar: ${jar}")
            list(APPEND jars ${jar})
          endif()
        endif()
      endif()
    endforeach()
    set(args ${args} INCLUDE_JARS ${jars} ${TDAQ_CLASSPATH})
  else()
    # set(args ${args} INCLUDE_JARS ${TDAQ_CLASSPATH})    
  endif()

  if(ARG_DAL)
    if(TARGET DAL_${TDAQ_PACKAGE_NAME})
      list(APPEND deps DAL_${TDAQ_PACKAGE_NAME})
    endif()
  endif()

  add_jar(${args})
  if(NOT "${deps}" STREQUAL "")
    add_dependencies(${target} ${deps})
  endif()

  # Create additional custom target with well-defined name
  # JAR_<package>_<target.jar>

  set(outname ${target})
  if(ARG_OUTPUT_NAME)
    set(outname ${ARG_OUTPUT_NAME})
  endif()

  find_program(FIX_JARS_COMMAND NAMES fix_jars.sh  PATHS ${TDAQ_CMAKE_DIR}/cmake/scripts NO_DEFAULT_PATH)

  # Find real location, in case 
  get_target_property(_jar_path ${target} JAR_FILE)

  add_custom_command(OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_FILES_DIRECTORY}/${target}.dir/java_class_extras
    COMMAND ${FIX_JARS_COMMAND}
    COMMAND ${Java_JAR_EXECUTABLE} uf ${_jar_path} @java_class_extras
    DEPENDS ${target} ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_FILES_DIRECTORY}/${target}.dir/java_class_filelist
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_FILES_DIRECTORY}/${target}.dir
    )

  add_custom_target(JAR_${TDAQ_PACKAGE_NAME}_${outname} ALL
    DEPENDS ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_FILES_DIRECTORY}/${target}.dir/java_class_extras)
  add_dependencies(JAR_${TDAQ_PACKAGE_NAME}_${outname} ${target})
  
  if(NOT ARG_NOINSTALL)
    tdaq_install_jar(${target} DESTINATION share/lib COMPONENT ${TDAQ_PACKAGE_NAME}_noarch-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION})
  endif()

endfunction()

#######################################################################
# tdaq_add_data(files... [DESTINATION dir][PACKAGE pkg])
#
# Add files to share/data/<package>/ with optional subdirectory
# With the PACKAGE option you can also pretend to be another package.
#######################################################################
function(tdaq_add_data)
  cmake_parse_arguments(ARG "" "DESTINATION;PACKAGE" "" ${ARGN})
  file(GLOB files ${ARG_UNPARSED_ARGUMENTS})

  if(ARG_PACKAGE)
    set(destpkg ${ARG_PACKAGE})
  else()
    set(destpkg ${TDAQ_PACKAGE_NAME})
  endif()

  install(FILES ${files} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/data/${destpkg}/${ARG_DESTINATION})
endfunction()

#######################################################################
# tdaq_add_docs(files... [DESTINATION dir][PACKAGE pkg])
#
# Add files to share/data/<package>/ with optional subdirectory
# With the PACKAGE option you can also pretend to be another package.
#######################################################################
function(tdaq_add_docs)
  cmake_parse_arguments(ARG "" "DESTINATION;PACKAGE" "" ${ARGN})
  file(GLOB files ${ARG_UNPARSED_ARGUMENTS})

  if(ARG_PACKAGE)
    set(destpkg ${ARG_PACKAGE})
  else()
    set(destpkg ${TDAQ_PACKAGE_NAME})
  endif()
	
  install(FILES ${files} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/doc/${destpkg}/${ARG_DESTINATION})
endfunction()

#######################################################################
# tdaq_add_examples(files... [DESTINATION dir])
#
# Add files to share/examples/<package>/ with optional subdirectory
#######################################################################
function(tdaq_add_examples)
  cmake_parse_arguments(ARG "" "DESTINATION" "" ${ARGN})
  file(GLOB files ${ARG_UNPARSED_ARGUMENTS})

  install(FILES ${files} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/examples/${TDAQ_PACKAGE_NAME}/${ARG_DESTINATION})
endfunction()

#######################################################################
# tdaq_add_db(file1 file2 DESTINATION subdir)
#
# install files into OKS DB area, subdir should one of 'hw', 'sw''
# 'segment', 'partition' etc. Files are globbed.
#
#######################################################################
function(tdaq_add_db)
  cmake_parse_arguments(ARG "" "DESTINATION" "" ${ARGN})
  if(NOT ARG_DESTINATION)
    messag(STATUS "ERROR: DESTINATION argument for tdaq_add_db is required")
  endif()
  file(GLOB files ${ARG_UNPARSED_ARGUMENTS})
  install(FILES ${files} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/data/${TDAQ_DB_PROJECT}/${ARG_DESTINATION})
endfunction()

# ######################################################################
# tdaq_add_schema(schema1 schema2...)
#
# Install a list of schema files into the default location 
# share/data/daq/schema/
#
# ######################################################################
function(tdaq_add_schema)
  tdaq_add_db(DESTINATION schema ${ARGN})
endfunction(tdaq_add_schema)

# ######################################################################
#
# tdaq_add_is_schema(schema1 schema2... [DESTINATION subdir] [PACKAGE pkg])
#
# Install ISInfo schema files into default location.
# You change the destination directory with the DESTINATION <subdir> option
# You pretend to be another package with the PACKAGE <pkg> option. In this
# case the file goes to share/data/${pkg}/${destination}
# ######################################################################
function(tdaq_add_is_schema)
  cmake_parse_arguments(ARG "" "DESTINATION;PACKAGE" "" ${ARGN})
  if(ARG_PACKAGE)
    set(destpkg ${ARG_PACKAGE})
  else()
    set(destpkg ${TDAQ_PACKAGE_NAME})
  endif()
  install(FILES ${ARG_UNPARSED_ARGUMENTS} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/data/${destpkg}/${ARG_DESTINATION})
  foreach(f ${ARG_UNPARSED_ARGUMENTS})
    get_filename_component(result ${f} NAME)
    list(APPEND TDAQ_IS_INFO_FILES share/data/${destpkg}/${ARG_DESTINATION}/${result})
  endforeach()
  set(TDAQ_IS_INFO_FILES ${TDAQ_IS_INFO_FILES} PARENT_SCOPE)
endfunction()

#######################################################################
#
# tdaq_add_header_directory(dir1 dir2 [DESTINATION subdir][PATTERN pattern][FILES file1 file2...])
#
# Install files into include area with optional subdirectory.
#
#######################################################################
function(tdaq_add_header_directory)
  cmake_parse_arguments(ARG "" "DESTINATION" "FILES;PATTERN" ${ARGN})

  foreach(f ${ARG_FILES})
    set(pattern ${pattern} PATTERN ${f})
  endforeach()

  foreach(f ${ARG_PATTERN})
    set(pattern ${pattern} PATTERN ${f})
  endforeach()
  
  if(NOT ARG_PATTERN AND NOT ARG_FILES)
    set(pattern PATTERN *.h PATTERN *.hh PATTERN *.hpp)
  endif()

  foreach(d ${ARG_UNPARSED_ARGUMENTS})
    install(DIRECTORY ${d} OPTIONAL COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION include/${ARG_DESTINATION} FILES_MATCHING ${pattern} PATTERN .git EXCLUDE PATTERN .svn EXCLUDE)
  endforeach()

endfunction()

function(tdaq_add_pcm)
  set(pcms)
  foreach(pcm ${ARGN})
    list(APPEND pcms ${CMAKE_CURRENT_BINARY_DIR}/${pcm}_rdict.pcm)
  endforeach()
  install(FILES ${pcms} OPTIONAL DESTINATION ${BINARY_TAG}/lib COMPONENT ${TDAQ_COMPONENT_BINARY})
endfunction()

# ######################################################################
# Download an arbitrary file into build area, maybe unpack it.
# If a relative URL is given, prefix it with ${TDAQ_DATA_URL}
# If it ends with .tar.gz, unpack it and interpret the output file name
# as directory name.
# ######################################################################
function(tdaq_get_data url output)

  if(EXISTS ${CMAKE_CURRENT_BINARY_DIR}/${output})
   return()
  endif()

  if(NOT ${url} MATCHES "https?://")
    set(url "${TDAQ_DATA_URL}/${url}")
  endif()

  message(STATUS "Downloading: ${url} to ${output}")
  
  if(${url} MATCHES ".*\\.tar" OR ${url} MATCHES ".*\\.tar.gz" OR ${url} MATCHES ".*\\.zip")
    get_filename_component(filename ${url} NAME)
    file(DOWNLOAD ${url} ${CMAKE_CURRENT_BINARY_DIR}/${filename} STATUS result)
    list(GET result 0 status)
    if(${status} EQUAL 0)
      file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${output})
      execute_process(COMMAND ${CMAKE_COMMAND} -E tar zxf ${CMAKE_CURRENT_BINARY_DIR}/${filename}
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${output})
      file(REMOVE ${CMAKE_CURRENT_BINARY_DIR}/${tmpname}.tar.gz)
      else()
        list(GET result 1 msg)
        message(WARNING "Download of ${url} failed: ${msg}")
    endif()
  else()
    file(DOWNLOAD ${url} ${CMAKE_CURRENT_BINARY_DIR}/${output})
  endif()

endfunction()

# ######################################################################
# Copied from Qt4Macros.cmake, if tdaq_ui_output_name(output input) is 
# a function  or macro, call it to convert the input name to an output 
# name. This function should look like this and be available in the 
# local CMakeLists.txt file:
# 
#    macro(tdaq_ui_output_name output input)
#       set(${output} ${CMAKE_CURRENT_BINARY_DIR}/${input}_ui.h)
#    endmacro()
#
# TDAQ_QT4_WRAP_UI(outfiles inputfile ... )
#
# ######################################################################

macro (TDAQ_QT4_WRAP_UI outfiles )
  QT4_EXTRACT_OPTIONS(ui_files ui_options ui_target ${ARGN})

  foreach (it ${ui_files})
    get_filename_component(outfile ${it} NAME_WE)
    get_filename_component(infile ${it} ABSOLUTE)
    if(COMMAND tdaq_ui_output_name)
      tdaq_ui_output_name(outfile ${outfile})
    else()
      set(outfile ${CMAKE_CURRENT_BINARY_DIR}/ui_${outfile}.h)
    endif()
    add_custom_command(OUTPUT ${outfile}
      COMMAND ${QT_UIC_EXECUTABLE}
      ARGS ${ui_options} -o ${outfile} ${infile}
      MAIN_DEPENDENCY ${infile} VERBATIM)
    set(${outfiles} ${${outfiles}} ${outfile})
  endforeach ()

endmacro ()

#
# TDAQ_QT4_ADD_RESOURCES(outfiles inputfile ... )
# minor change compared to original: switch rcc options and -name ... on command line
# to allow the user to override the latter.
#

macro (TDAQ_QT4_ADD_RESOURCES outfiles )
  QT4_EXTRACT_OPTIONS(rcc_files rcc_options rcc_target ${ARGN})

  foreach (it ${rcc_files})
    get_filename_component(outfilename ${it} NAME_WE)
    get_filename_component(infile ${it} ABSOLUTE)
    get_filename_component(rc_path ${infile} PATH)
    set(outfile ${CMAKE_CURRENT_BINARY_DIR}/qrc_${outfilename}.cxx)

    set(_RC_DEPENDS)
    if(EXISTS "${infile}")
      #  parse file for dependencies
      #  all files are absolute paths or relative to the location of the qrc file
      file(READ "${infile}" _RC_FILE_CONTENTS)
      string(REGEX MATCHALL "<file[^<]+" _RC_FILES "${_RC_FILE_CONTENTS}")
      foreach(_RC_FILE ${_RC_FILES})
        string(REGEX REPLACE "^<file[^>]*>" "" _RC_FILE "${_RC_FILE}")
        if(NOT IS_ABSOLUTE "${_RC_FILE}")
          set(_RC_FILE "${rc_path}/${_RC_FILE}")
        endif()
        set(_RC_DEPENDS ${_RC_DEPENDS} "${_RC_FILE}")
      endforeach()
      unset(_RC_FILES)
      unset(_RC_FILE_CONTENTS)
      # Since this cmake macro is doing the dependency scanning for these files,
      # let's make a configured file and add it as a dependency so cmake is run
      # again when dependencies need to be recomputed.
      QT4_MAKE_OUTPUT_FILE("${infile}" "" "qrc.depends" out_depends)
      configure_file("${infile}" "${out_depends}" COPYONLY)
    else()
      # The .qrc file does not exist (yet). Let's add a dependency and hope
      # that it will be generated later
      set(out_depends)
    endif()

    add_custom_command(OUTPUT ${outfile}
      COMMAND ${QT_RCC_EXECUTABLE}
      ARGS -name ${outfilename} ${rcc_options} -o ${outfile} ${infile}
      MAIN_DEPENDENCY ${infile}
      DEPENDS ${_RC_DEPENDS} "${out_depends}" VERBATIM)
    set(${outfiles} ${${outfiles}} ${outfile})
  endforeach ()

endmacro ()

# functions to generate the information for dal_create_sw_repository

# Add an IGUI property to software repository
#
# tdaq_add_igui_properties(-Digui.foo.bar=bar -Dsomething.else=foo)
#
function(tdaq_add_igui_properties)
  list(APPEND TDAQ_IGUI_PROPERTIES ${ARGN})
  set(TDAQ_IGUI_PROPERTIES ${TDAQ_IGUI_PROPERTIES} PARENT_SCOPE)
endfunction()

# Add a package variable to the software repository
#
# tdaq_add_package_variable(name [NAME other_name] [DESCRIPTION "description"] [SUFFIX suffix])
#
function(tdaq_add_package_variable name)
  cmake_parse_arguments(ARG "" "SUFFIX;NAME;DESCRIPTION" "" ${ARGN})
  if(ARG_NAME)
    tdaq_sw_repo("${ARG_NAME}@SW_PackageVariable:")
  else()
    tdaq_sw_repo("${name}@Variable:")
  endif()
  if(ARG_SUFFIX)
    tdaq_sw_repo("  Suffix: ${ARG_SUFFIX}")
  endif()
  if(ARG_DESCRIPTION)
    tdaq_sw_repo("  Description: \"${ARG_DESCRIPTION}\"")
  endif()
endfunction()

function(tdaq_add_external_package name)
  cmake_parse_arguments(ARG "" "DESCRIPTION;INSTALLPATH;TAG;PATCHAREA" "SHARED_LIBRARIES;BINARIES;USES;NEEDS_ENVIRONMENT;PROCESS_ENVIRONMENT" ${ARGN})

  if(NOT ARG_INSTALLPATH)
    message(WARNING "INSTALL_PATH argument to tdaq_external_package(${name}) is required")
    return()
  endif()

  tdaq_sw_repo("${name}@SW_ExternalPackage:")
  tdaq_sw_repo("  InstallationPath: ${ARG_INSTALLPATH}")

  if(ARG_PATCHAREA)
    tdaq_sw_repo("  PatchArea: ${ARG_PATCHAREA}")
  endif()

  if(ARG_DESCRIPTION)
    tdaq_sw_repo("  Description: \"${ARG_DESCRIPTION}\"")
  endif()

  if(ARG_TAG)
    tdaq_sw_repo("  Tag: ${ARG_TAG}")
  endif()

  if(ARG_SHARED_LIBRARIES)
    tdaq_sw_repo("  SharedLibraries:")
    foreach(mapping ${ARG_SHARED_LIBRARIES})
      tdaq_sw_repo("   - ${mapping}")
    endforeach()
  endif()

  if(ARG_BINARIES)
    tdaq_sw_repo("  Binaries:")
    foreach(mapping ${ARG_BINARIES})
      tdaq_sw_repo("   - ${mapping}")
    endforeach()
  endif()

  if(ARG_USES)
    tdaq_sw_repo("  Uses:")
    foreach(use ${ARG_USES})
      tdaq_sw_repo("    - ${use}")
    endforeach()
  endif()

  if(ARG_NEEDS_ENVIRONMENT)
    tdaq_sw_repo("  Needs:")
    foreach(env ${ARG_NEEDS_ENVIRONMENT})
      tdaq_sw_repo("    - ${env}")
    endforeach()
  endif()

  if(ARG_PROCESS_ENVIRONMENT)
    tdaq_sw_repo("  ProcessEnvironment:")
    foreach(env ${ARG_PROCESS_ENVIRONMENT})
      tdaq_sw_repo("    - ${env}")
    endforeach()
  endif()

endfunction()

function(tdaq_add_environment_variable name)
  cmake_parse_arguments(ARG "DEFAULT_TAG_MAPPINGS" "VALUE;DESCRIPTION" "" ${ARGN})

  tdaq_sw_repo("${name}@Variable:")
  if(ARG_VALUE)
    tdaq_sw_repo("  Value: ${ARG_VALUE}")
  else()
    message(WARNING "tdaq_add_environment_variable(${name} ....) misses VALUE argument")
  endif()
  if(ARG_DESCRIPTION)
    tdaq_sw_repo("  Description: \"${ARG_DESCRIPTION}\"")
  endif()

  if(ARG_DEFAULT_TAG_MAPPINGS)
    tdaq_sw_repo("  TagMapping:")
    foreach(tag ${TDAQ_TAGS})
      tdaq_sw_repo("    - ${ARG_VALUE}/${tag}")
    endforeach()
  endif()

endfunction()

######################################################################
# Add a script or a binary to the sw repository
######################################################################
function(tdaq_add_to_repo program)

  cmake_parse_arguments(ARG "BINARY;SCRIPT" "NAME;DESCRIPTION;URL;SHELL" "PARAMETERS;RESOURCES;ENVIRONMENT;USES;INCLUDES" ${ARGN})
  if(NOT ARG_BINARY AND NOT ARG_SCRIPT)
    message(STATUS "Warning: One of BINARY or SCRIPT flag should be provided")
  endif()

  if(ARG_BINARY)
    set(type Binary)
    # Should get OUTPUT_NAME property really, if it's a TARGET
    set(exec ${program})
  else()
    set(type Script)
    get_filename_component(exec ${program} NAME)
  endif()

  if(ARG_NAME)
    tdaq_sw_repo("${ARG_NAME}@${type}:")
  else()
    tdaq_sw_repo("${exec}@${type}:")
  endif()

  tdaq_sw_repo("  BinaryName: ${exec}")

  if(ARG_DESCRIPTION)
    tdaq_sw_repo("  Description: \"${ARG_DESCRIPTION}\"")
  endif()

  if(ARG_URL)
    tdaq_sw_repo("  HelpURL: ${ARG_URL}")
  endif()

  if(ARG_SHELL)
    tdaq_sw_repo("  Shell: ${ARG_SHELL}")
  endif()

  if(ARG_PARAMETERS)
    tdaq_sw_repo("  Parameters: ${ARG_PARAMETERS}")
  endif()

  if(ARG_RESOURCES)
    tdaq_sw_repo("  Resources:")
    foreach(p ${ARG_RESOURCES})
      tdaq_sw_repo("    - ${p}")
    endforeach()
  endif()

  if(ARG_ENVIRONMENT)
    tdaq_sw_repo("  Environment:")
    foreach(p ${ARG_ENVIRONMENT})
      tdaq_sw_repo("    - ${p}")
    endforeach()
  endif()

  if(ARG_USES)
    tdaq_sw_repo("  Uses:")
    foreach(p ${ARG_USES})
      tdaq_sw_repo("    - ${p}")
    endforeach()
  endif()

  if(ARG_INCLUDES)
    tdaq_sw_repo("  Includes:")
    foreach(p ${ARG_INCLUDES})
      tdaq_sw_repo("    - ${p}")
    endforeach()
  endif()

endfunction()

function(tdaq_add_jar_to_repo jar)

  cmake_parse_arguments(ARG "" "NAME;DESCRIPTION;URL" "RESOURCES;USES" ${ARGN})

  if(ARG_NAME)
    tdaq_sw_repo("${ARG_NAME}@JarFile:")
  else()
    tdaq_sw_repo("${jar}@JarFile:")
  endif()

  # Should use properties of jar target
  tdaq_sw_repo("  BinaryName: ${jar}")

  if(ARG_DESCRIPTION)
    tdaq_sw_repo("  Description: \"${ARG_DESCRIPTION}\"")
  endif()

  if(ARG_URL)
    tdaq_sw_repo("  URL: ${ARG_URL}")
  endif()

  if(ARG_RESOURCES)
    tdaq_sw_repo("  Resources:")
    foreach(p ${ARG_RESOURCES})
      tdaq_sw_repo("    - ${p}")
    endforeach()
  endif()

  if(ARG_USES)
    tdaq_sw_repo("  Uses:")
    foreach(p ${ARG_USES})
      tdaq_sw_repo("    - ${p}")
    endforeach()
  endif()

endfunction()

function(tdaq_add_software_repository name)

  cmake_parse_arguments(ARG "" "INSTALLATIONPATH;INSTALLATIONPATHVARIABLENAME;PATCHAREA" "USES" ${ARGN})

  tdaq_sw_repo("${name}@SW_Repository:")
  tdaq_sw_repo("  Name: ${name}")

  if(ARG_INSTALLATIONPATH)
    tdaq_sw_repo("  InstallationPath: ${ARG_INSTALLATIONPATH}")
  endif()

  if(ARG_INSTALLATIONPATHVARIABLENAME)
    tdaq_sw_repo("  InstallationPathVariableName: ${ARG_INSTALLATIONPATHVARIABLENAME}")
  endif()

  if(ARG_PATCHAREA)
    tdaq_sw_repo("  PatchArea: ${ARG_PATCHAREA}")
  endif()

  if(ARG_USES)
    tdaq_sw_repo("  Uses:")
    foreach(use ${ARG_USES})
      tdaq_sw_repo("    - ${use}")
    endforeach()
  endif()

endfunction()


# last resort, install an arbitrary file
# but please tell us if it's binary or noarch
function(tdaq_add_file)

  cmake_parse_arguments(ARG "BINARY;NOARCH" "DESTINATION" "" ${ARGN})

  if(NOT (ARG_BINARY OR ARG_NOARCH) OR (ARG_BINARY AND ARG_NOARCH))
    message(SEND_ERROR "tdaq_add_file() requires either BINARY or NOARCH flag")
    return()
  endif()

  if(ARG_BINARY)
    set(comp ${TDAQ_COMPONENT_BINARY})
  elseif(ARG_NOARCH)
    set(comp ${TDAQ_COMPONENT_NOARCH})
  endif()

  install(FILES ${ARG_UNPARSED_ARGUMENTS} OPTIONAL COMPONENT ${comp} DESTINATION ${ARG_DESTINATION})

endfunction()

function(tdaq_add_test)

  cmake_parse_arguments(ARG "POST_INSTALL" "NAME;WORKING_DIRECTORY" "COMMAND;CONFIGURATIONS" ${ARGN})
  set(test_name ${TDAQ_PACKAGE_NAME}_${ARG_NAME})
  if(ARG_POST_INSTALL)
    set(args NAME ${test_name} COMMAND ${TDAQ_RUNNER} ${ARG_COMMAND})
  else()
    set(args NAME ${test_name} COMMAND ${ARG_COMMAND})
  endif()

  if(ARG_WORKING_DIRECTORY)
    set(args ${args} WORKING_DIRECTORY ${ARG_WORKING_DIRECTORY})
  endif()
  if(ARG_CONFIGURATIONS)
    set(args ${args} CONFIGURATIONS ${ARG_CONFIGURATIONS})
  endif()

  add_test(${args})

  if(ARG_POST_INSTALL)
    set_tests_properties(${test_name} PROPERTIES LABELS post_install)
  else()
    set_tests_properties(${test_name} PROPERTIES LABELS pre_install)
  endif()

endfunction()

function(tdaq_show_info)

  cmake_parse_arguments(ARG "TARGETS;VARIABLES;CACHE;DEFINITIONS;OPTIONS;CXXFLAGS;CFLAGS;ALL" "" "" ${ARGN})

  if(ARG_TARGETS OR ARG_ALL)
    get_directory_property(targets DEFINITION TDAQ_TARGETS)
    message(STATUS "Targets")
    foreach(target ${targets})
      message(STATUS "  ${target}")
    endforeach()
  endif()

  if(ARG_VARIABLES OR ARG_ALL)
    get_directory_property(variables VARIABLES)
    message(STATUS "Variables")
    foreach(var ${variables})
      if(${var} MATCHES "_.*")
        continue()
      endif()
      message(STATUS "  ${var} = ${${var}}")
    endforeach()
  endif()

  if(ARG_CACHE OR ARG_ALL)
    get_directory_property(variables CACHE_VARIABLES)
    message(STATUS "Cache")
    foreach(var ${variables})
      if(${var} MATCHES "_.*")
        continue()
      endif()
      message(STATUS "  ${var} = ${${var}}")
    endforeach()
  endif()

  if(ARG_DEFINITIONS OR ARG_ALL)
    get_directory_property(variables COMPILE_DEFINITIONS)
    message(STATUS "Compile Definitions")
    foreach(var ${variables})
      message(STATUS "  ${var}")
    endforeach()
  endif()

  if(ARG_OPTIONS OR ARG_ALL)
    get_directory_property(variables COMPILE_OPTIONS)
    message(STATUS "Compile Options")
    foreach(var ${variables})
      message(STATUS "  ${var}")
    endforeach()
  endif()

  if(ARG_CXXFLAGS OR ARG_ALL)
    string(TOUPPER ${CMAKE_BUILD_TYPE} upper)
    message(STATUS "CXXFLAGS = ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${upper}}")
  endif()

  if(ARG_CFLAGS OR ARG_ALL)
    string(TOUPPER ${CMAKE_BUILD_TYPE} upper)
    message(STATUS "CFLAGS = ${CMAKE_C_FLAGS} ${CMAKE_C_FLAGS_${upper}}")
  endif()

endfunction()

function(tdaq_find_jar RESULT)
    cmake_parse_arguments(ARG "" "" "VERSIONS;PATHS;NAMES"  ${ARGN})

    set(paths ${JAVA_ROOT}/jre/lib)

    if(ARG_PATHS)
      set(paths ${ARG_PATHS} ${paths})
    endif()

    if(NOT ARG_NAMES)
      set(ARG_NAMES ${ARG_UNPARSED_ARGUMENTS})
    endif()

    set(_jar_names)
    
    foreach(_name ${ARG_NAMES})
      if(ARG_VERSIONS)
        foreach(_version ${ARG_VERSIONS})
          list(APPEND _jar_names ${_name}-${_version}.jar)
        endforeach()
      else()
        list(APPEND _jar_names ${_name}.jar)
      endif()
    endforeach()
    find_file(${RESULT} NAMES ${_jar_names} PATHS ${paths} DOC "Jar file location" NO_DEFAULT_PATH)

endfunction()

function(tdaq_add_package_labels)
  set(TDAQ_PACKAGE_LABELS ${TDAQ_PACKAGE_LABELS} ${ARGN} PARENT_SCOPE)
endfunction()

function(tdaq_find_lcg_python pkg)
  cmake_parse_arguments(ARG "AS_LIST" "PYTHONPATH;LD_LIBRARY_PATH;PATH" "" ${ARGN})

  string(TOUPPER ${pkg} PKG)

  if(${PKG}_ROOT)

    set(_out_python)
    set(_out_ld)
    set(_out_path)

    if(${PKG}_DEPENDENCIES)
      foreach(_dep ${${PKG}_DEPENDENCIES})
        set(_python)
        set(_ld)
        set(_path)
        tdaq_find_lcg_python( ${_dep} PYTHONPATH _python LD_LIBRARY_PATH _ld PATH _path AS_LIST)
        if(_python)
          list(INSERT _out_python 0 ${_python})
        endif()
        if(_ld)
          list(INSERT _out_ld 0 ${_ld})
        endif()
        if(_path)
          list(INSERT _out_path 0 ${_path})
        endif()
      endforeach()
    endif()

    file(GLOB _python ${${PKG}_ROOT}/lib/python*/site-packages)

    if(ARG_PYTHONPATH)
      list(INSERT _out_python 0 ${_python})
      list(REMOVE_DUPLICATES _out_python)
      if(NOT ARG_AS_LIST)
        string(REPLACE ";" ":" _out_python "${_out_python}")
      endif()
      set(${ARG_PYTHONPATH} ${_out_python} PARENT_SCOPE)
    endif()

    if(ARG_LD_LIBRARY_PATH)
      list(INSERT _out_ld 0 ${${PKG}_ROOT}/lib)
      list(INSERT _out_ld 0 ${_out_python})
      list(REMOVE_DUPLICATES _out_ld)
      if(NOT ${ARG_AS_LIST})
        string(REPLACE ";" ":" _out_ld "${_out_ld}")
      endif()
      set(${ARG_LD_LIBRARY_PATH} ${_out_ld} PARENT_SCOPE)
    endif()

    if(ARG_PATH)
      list(INSERT _out_path 0 ${${PKG}_ROOT}/bin)
      list(REMOVE_DUPLICATES _out_path)
      if(NOT ${ARG_AS_LIST})
        string(REPLACE ";" ":" _out_path "${_out_path}")
      endif()
      set(${ARG_PATH} ${_out_path} PARENT_SCOPE)
    endif()

  endif()

endfunction()

# set(BINARY_TAG $ENV{BINARY_TAG} CACHE STRING "Platform ID")
