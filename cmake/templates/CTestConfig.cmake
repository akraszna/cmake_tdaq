set(CTEST_PROJECT_NAME "@PROJECT@")
set(CTEST_NIGHTLY_START_TIME "00:00:00 CEST")

set(CTEST_SUBMIT_URL "http://atlas-tdaq-code.cern.ch/cdash/submit.php?project=@PROJECT@")

set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "atlas-tdaq-code.cern.ch")
set(CTEST_DROP_LOCATION "/cdash/submit.php?project=@PROJECT@")
set(CTEST_DROP_SITE_CDASH TRUE)

set(CTEST_USE_LAUNCHERS 1)
