#!/usr/bin/env python
#
# The behaviour of this post build step can be influenced by environment variables:
# 
# TDAQ_EMAIL_ERRORS=1                                              Automated e-mails on build errors are sent to developers.
# TDAQ_EMAIL_RECIPIENTS=mail@domain.com,mail2@domain2.com,...      E-mails are sent only to this specific addresses.
# TDAQ_EMAIL_EXTRA_RECIPIENTS=mail@domain.com,mail2@domain2.com... E-mails are sent to these addresses in addition to developers.
#
# To get the association between package and developers, this script relies on a file
#
#   https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/authors/authors.txt
# 
# that is supposed to contain an up-to-date list. Since the generation of this file takes several
# minutes it is done by an independent acron job instead of inside the build itself.
#
import sys
import re
import string
import urllib
from xml.dom import minidom
from os import popen, getenv, path

doc = minidom.parse(sys.argv[1])
tests = minidom.parse(path.dirname(sys.argv[1]) + '/Test.xml')
source = sys.argv[2]
binary = sys.argv[3]

def compare(a,b):
   if a['Error'] == b['Error']: return cmp(b['Warning'], a['Warning'])
   return cmp(b['Error'], a['Error'])

failures = {}

for c in doc.getElementsByTagName('Failure'):
    t = c.getAttribute('type')
    if not t in ['Warning', 'Error']: continue
    wd = c.getElementsByTagName('WorkingDirectory')[0].firstChild.nodeValue
    if wd == binary or wd == source: continue
    try:
       pkg = wd.replace(binary,'').replace(source,'').split('/')[1]
    except:
       continue
    if not failures.has_key(pkg):
       failures[pkg] = { 'Name': pkg, 'Warning': 0, 'Error': 0 , 'Files': {}, 'Tests': 0}
    failures[pkg][t] += 1
    source_file = c.getElementsByTagName('SourceFile')
    if source_file:
      source_file = source_file[0].firstChild.nodeValue
      if not failures[pkg]['Files'].has_key(source_file):
        failures[pkg]['Files'][source_file] = { 'Warning': 0, 'Error': 0 } 
      failures[pkg]['Files'][source_file][t] += 1

for c in tests.getElementsByTagName('Test'):
   if c.getAttribute('Status') == 'failed':
      pkg = c.getElementsByTagName('Path')[0].firstChild.nodeValue.split('/')[-1]
      if not failures.has_key(pkg):
         failures[pkg] = { 'Name': pkg, 'Warning': 0, 'Error': 0 , 'Files': {}, 'Tests': 0}
      failures[pkg]['Tests'] += 1

fail_list = [ f for f in failures.values() ]
fail_list.sort(cmp=compare)

print '''<html>
<head>
  <style>
    .Error {
       background-color: #F1CE68;
    }
  </style>
  <title>Errors and warnings per package</title>
  <script type="text/javascript" src="../../javascript/jquery-1.6.2.js"></script> 
  <script type="text/javascript" src="../../javascript/jquery.tablesorter.js"></script> 
  <script type="text/javascript">
     $(document).ready(function() 
    { 
        $("#theTable").tablesorter(); 
    } 
); 
  </script>
</head>
<body>
<h1>Error and Warning Summary per Package</h1>
'''

print '<table id="theTable" class="tablesorter" frame="border" rules="all" ><thead><tr><th>Package</th><th>Errors</th><th>Warnings</th><th>Commit</th><th>Files</th></tr></thead><tbody>'

error_packages   = [ '<a href="#%s">%s</a>' % (out['Name'], out['Name']) for out in fail_list if out['Error'] > 0]
warning_packages = [ '<a href="#%s">%s</a>' % (out['Name'], out['Name']) for out in fail_list if out['Error'] == 0]

error_packages.sort()
warning_packages.sort()

print '<p><b>Errors in:</b><br>',string.join(error_packages,', ')
print
print '<p><b>Warnings in:</b><br>', string.join(warning_packages,', ')

pattern = re.compile('commit (.*)\n')
email   = re.compile('Author: (.*)<(.*)>')

for out in fail_list:
   log = popen('cd %s; git log --format=medium -1' % (source + '/' + out['Name']),'r')
   log_txt = log.read()
   log_txt = re.sub(pattern, 'commit <a href="https://gitlab.cern.ch/atlas-tdaq-software/%s/commit/\\1">\\1</a>\n' % out['Name'], log_txt)
   log_txt = re.sub(email, 'Author: \\1<a href="mailto:\\2">&lt;\\2&gt;<a>', log_txt)
   
   print '<tr class="%s">' % (out['Error'] > 0 and 'Error' or 'Warning'),'<th><a name="%s" href="https://gitlab.cern.ch/atlas-tdaq-software/%s">' % (out['Name'], out['Name']),out['Name'],"</a></th><td>",out['Error'],'</td><td>',out['Warning'],'</td><td><pre>' + log_txt,'</pre></td><td>'
   for s in out['Files']:
     print '<code>'+ s + '</code>' + ':',out['Files'][s]['Error'],'error(s)',out['Files'][s]['Warning'],'warning(s)<br>'
   print "</tr>"
   log.close()
print "</tbody></body></html>"

# prepare the e-mail submissions
pkg_devs = {}
emails = {}

if getenv('TDAQ_EMAIL_ERRORS', None) == '1':

   startTime = doc.getElementsByTagName('StartDateTime')[0].firstChild.nodeValue

   # read authors file and build dictionary of package/authorlist
   for line in urllib.urlopen('https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/authors/authors.txt').readlines():
      try:
         pkg, authors = line.strip().split(' ',1)
      except:
         continue
      pkg_devs[pkg] = authors.split(',')

   recipients = getenv('TDAQ_EMAIL_RECIPIENTS',None)
   if recipients:
      for pkg in pkg_devs:
         pkg_devs[pkg] = recipients.split(',')

   recipients = getenv('TDAQ_EMAIL_EXTRA_RECIPIENTS',None)
   if recipients:
      for pkg in pkg_devs:
         pkg_devs[pkg] += recipients.split(',')

   for pkg in fail_list:
      if pkg['Error'] > 0 or pkg['Tests'] > 0:
         if not pkg_devs.has_key(pkg['Name']): continue
         for author in pkg_devs[pkg['Name']]:
            if emails.has_key(author):
               emails[author] += '%s:%s%d error(s) \t%d warning(s) \t%d failed test(s)\n' % (pkg['Name'], (25 - len(pkg['Name'])) * ' ', pkg['Error'], pkg['Warning'], pkg['Tests'])
            else:
               emails[author] = '%s:%s%d error(s) \t%d warning(s) \t%d failed test(s)\n' % (pkg['Name'],  (25 - len(pkg['Name'])) * ' ', pkg['Error'], pkg['Warning'], pkg['Tests'])

for e in emails:
   if e == 'unknown@cern.ch': continue

   msg = "Dear TDAQ developer,\n\n"
   msg += "You have one or more build or test errors in one of the nightly or continuous\n"
   msg += "TDAQ project builds to which you have at least developer access rights.\n"
   msg += "For more details see:\n\n"
   msg += "https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/cdash/index.php?project=" +source.split('/')[-2] + '\n\n'
   version =  "Summary for " + source.split('/')[-1] + ' ' + binary.split('/')[-1] + '  '+ startTime
   msg += version + '\n'
   msg += ('=' * len(version)) + '\n\n'
   msg += emails[e]
   msg += "\n\nThis is an automated e-mail, please don't reply"
   m = popen('mail -s "TDAQ Build Errors" %s' % e, 'w')
   print>>m,msg
   m.close()
