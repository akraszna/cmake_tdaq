#
# This is a ctest script to build a project from scratch. It assumes
# that is is called from build_release with various environment variables
# properly set.
# CHECKOUT_AREA, BUILD_AREA
# PROJECT, BRANCH, CONFIG
#
set(CTEST_SOURCE_DIRECTORY "$ENV{CHECKOUT_AREA}/$ENV{PROJECT}/$ENV{BRANCH}")
set(CTEST_BINARY_DIRECTORY "$ENV{BUILD_AREA}/$ENV{PROJECT}/$ENV{BRANCH}/$ENV{CONFIG}")

if(NOT DEFINED CTEST_BUILD_NAME)
set(CTEST_BUILD_NAME "$ENV{BRANCH}-$ENV{CONFIG}")
endif()

set(CTEST_NIGHTLY_START_TIME "01:00:00 UTC")

if(NOT DEFINED CTEST_MODE)
set(CTEST_MODE Experimental)
endif()

if(NOT DEFINED CTEST_NOCLEAN)
ctest_empty_binary_directory(${CTEST_BINARY_DIRECTORY})
endif()

if(DEFINED CTEST_TRACK)
set(CTEST_TRACK TRACK ${CTEST_TRACK})
endif()

find_program(hostname NAMES hostname)
execute_process(COMMAND ${hostname} OUTPUT_VARIABLE CTEST_SITE OUTPUT_STRIP_TRAILING_WHITESPACE)

find_program(checkout_release NAMES checkout_release)
set(CTEST_CHECKOUT_COMMAND "${checkout_release} $ENV{CHECKOUT_OPTIONS} $ENV{PROJECT} $ENV{BRANCH}")

# set(CTEST_GIT_UPDATE_CUSTOM "${checkout_release}" ${PROJECT} ${BRANCH})

find_program(cmake_config NAMES cmake_config)
set(CTEST_CONFIGURE_COMMAND "${cmake_config} $ENV{CONFIG} ${CTEST_SOURCE_DIRECTORY} -- $ENV{CMAKE_OPTIONS}")

include(ProcessorCount)
ProcessorCount(NumCores)

if(${NumCores} GREATER 0)
set(CTEST_BUILD_FLAGS "-j ${NumCores}")
endif()

find_program(make NAMES make)
set(CTEST_BUILD_COMMAND "${make} ${CTEST_BUILD_FLAGS} -k all install/fast")

ctest_start(${CTEST_MODE} ${CTEST_TRACK})
ctest_read_custom_files(${CTEST_SOURCE_DIRECTORY}/cmake)
ctest_configure(QUIET)
ctest_read_custom_files(${CTEST_BINARY_DIRECTORY}/cmake)
ctest_build(QUIET)
if(NOT DEFINED CTEST_NOTESTS)
ctest_test(QUIET)
endif()

if(EXISTS ${CTEST_BINARY_DIRECTORY}/packages.txt)
set(CMAKE_EXTRA_SUBMIT_FILES ${CMAKE_EXTRA_SUBMIT_FILES} ${CTEST_BINARY_DIRECTORY}/packages.txt)
file(STRINGS ${CTEST_BINARY_DIRECTORY}/packages.txt packages)
file(WRITE ${CTEST_BINARY_DIRECTORY}/packages.html 
"<html><head><title>Packages</title></head><body><table frame=\"border\" rules=\"all\"><thead><tr><th>Package</th><th>Tag</th></tr></thead><tbody>\n")
   while(packages)
      list(GET packages 0 input_line)
      list(REMOVE_AT packages 0)
      string(REGEX MATCHALL "[^ \t]+" parsed_line ${input_line})
      list(GET parsed_line 0 pkg_name)
      list(GET parsed_line 1 pkg_tag)
      file(APPEND ${CTEST_BINARY_DIRECTORY}/packages.html "<tr><th>${pkg_name}</th><td>${pkg_tag}</td></tr>\n")
   endwhile()
   file(APPEND ${CTEST_BINARY_DIRECTORY}/packages.html "</tbody></table></body></html>\n")
   set(CMAKE_EXTRA_SUBMIT_FILES ${CMAKE_EXTRA_SUBMIT_FILES} ${CTEST_BINARY_DIRECTORY}/packages.html)
endif()

get_filename_component(bindir ${cmake_config} DIRECTORY)
get_filename_component(cmake_tdaq_dir ${bindir}/.. ABSOLUTE)

find_program(head NAMES head)
execute_process(COMMAND ${head} -1  ${CTEST_BINARY_DIRECTORY}/Testing/TAG OUTPUT_VARIABLE tagdir OUTPUT_STRIP_TRAILING_WHITESPACE)
find_program(post_build NAMES post_build.py HINTS ${cmake_tdaq_dir}/cmake/scripts NO_SYSTEM_DEFAULT)
execute_process(COMMAND ${post_build} ${CTEST_BINARY_DIRECTORY}/Testing/${tagdir}/Build.xml ${CTEST_SOURCE_DIRECTORY} ${CTEST_BINARY_DIRECTORY} OUTPUT_VARIABLE perPackage)
file(WRITE ${CTEST_BINARY_DIRECTORY}/PerPackage.html ${perPackage})
set(CMAKE_EXTRA_SUBMIT_FILES ${CMAKE_EXTRA_SUBMIT_FILES} ${CTEST_BINARY_DIRECTORY}/PerPackage.html)

ctest_upload(FILES ${CMAKE_EXTRA_SUBMIT_FILES} QUIET)

ctest_submit(QUIET RETRY_COUNT 3)
