
This directory contains various helper scripts to manipulate 
a whole group on gitlab.cern.ch

add_runner <runner-id> [ <group> ]

   Add a specific runner to the whole group

add_access_token [ <group> ]

   Add an access token to each project and store the value in the 
   TDAQ_CI_TRIGGER variable

add_gitlab_ci <release>

   Add the gitlab-ci.yml.template to each project in the release
   if it does not already have one.
